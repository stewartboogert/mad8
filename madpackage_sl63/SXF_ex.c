/* extract SXF file from DOOM */
/* #define _call_tree_ */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#ifdef __LINUX__
#include "doomex.h"
#include "doom.h"
#endif
#ifndef __LINUX__
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doomex.h"
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doom.h"
#endif

/*---------------------------------------------------------------------*
*                                                                      *
*                           CERN                                       *
*                                                                      *
*     European Organization for Nuclear Research                       *
*                                                                      *
*     Program name: SXF: SXF Format Converter                          *
*                                                                      *
*     Author and contact:   Hans GROTE                                 *
*                           SL Division                                *
*                           CERN                                       *
*                           CH-1211 GENEVA 23                          *
*                           SWITZERLAND                                *
*                      Tel. [041] (022) 767 49 61                      *
*                           Hans.Grote@cern.ch                         *
*                                                                      *
*     Copyright  CERN,  Geneva  1990  -  Copyright  and  any   other   *
*     appropriate  legal  protection  of  this  computer program and   *
*     associated documentation reserved  in  all  countries  of  the   *
*     world.                                                           *
*                                                                      *
*     Organizations collaborating with CERN may receive this program   *
*     and documentation freely and without charge.                     *
*                                                                      *
*     CERN undertakes no obligation  for  the  maintenance  of  this   *
*     program,  nor responsibility for its correctness,  and accepts   *
*     no liability whatsoever resulting from its use.                  *
*                                                                      *
*     Program  and documentation are provided solely for the use  of   *
*     the organization to which they are distributed.                  *
*                                                                      *
*     This program  may  not  be  copied  or  otherwise  distributed   *
*     without  permission. This message must be retained on this and   *
*     any other authorized copies.                                     *
*                                                                      *
*     The material cannot be sold. CERN should be  given  credit  in   *
*     all references.                                                  *
*                                                                      *
*---------------------------------------------------------------------*/

#define LINE_MAX 78
#define ALIGN_MAX 6
#define FIELD_MAX 100
#define MAX_TYPE 11
#define MAX_TAG 50
#define ANG_POS 10
#define K0_POS 12

int  all_blank(char*);
char* bpad(char*, int);
void fill_dump(int, char*, double*, int, int, double);
void lower(char*);
void pro_elem(int);
void put_line(char*);
void r_indent();
void accu_line(char*);
void s_indent(int);
void sxf_init();
void sxf_out();
void sxf_rtag();
char* tag_spec(char*);
void reset_line();
void write_body(int);
void write_align();
void write_elend();
void write_field();
void write_elstart(int);
void write_header(char*);
void write_trailer(double);

struct object *pseq,              /* pointer to SEQUENCE object */
              *pelem,             /* pointer to current ELEMENT object */
              *palign,            /* pointer to current alignment object */
              *pfield,            /* pointer to current field error object */
              *pbase;             /* pointer to current BASE object */

char          sequ_name[KEY_LENGTH],
              key[KEY_LENGTH],
              line[LINE_MAX],
              aux[LINE_MAX],
              tag_type[MAX_TAG][16],
              tag_code[MAX_TAG][16],
              pad[MAX_TYPE+1];

int           sequ_no_elem,        /* no. of elements in sequence */
              na_err,              /* current no. of alignment errors */
              nf_err,              /* current no. of field errors */
              indent,              /* current indentation count */
              b_level,             /* current brace level */
              elem_cnt = 0,        /* element count */
              tag_flag = 0,        /* if > 0, tag = parent name written */
              tag_cnt = 0,         /* if > 0, tag = specified type code 
                                      written for selected types only */
              align_cnt = 0,       /* element with align errors count */
              field_cnt = 0,       /* element with field errors count */
              occnt_add = 0,       /* flag for element name modification */ 
              b_indent[100],       /* list of indents */
              add_indent[] = {1, 2, 2, 4, 7, 7, 7, 7, 7, 7};

double        sequ_length,         /* length of  sequence */
              sequ_start, 
              sequ_end,
              al_errors[ALIGN_MAX],
              fd_errors[FIELD_MAX];

FILE          *out, *ftag;

const double zero = 0;

void main(int argc, char *argv[])  /* writes an SXF machine file 
                                      extracted from a DOOM database */
{
  int snoup = 1;

  if (argc < 4) 
    {
     puts("SXF_ex usage: SXF_ex dbname sequ_name out_file <-tag>");
     exit(1);
    }
  strcpy(sequ_name, argv[2]);
  out = fopen(argv[3], "w");
  if (out == NULL)
    {
     printf("Fatal - cannot open output file: %s\n", argv[3]);
     exit(1);
    }
  if (argc > 4 && strcmp(argv[4], "-tag") == 0) 
    {
     tag_flag = 1;
     if (argc > 5) 
       {
        ftag = fopen(argv[5], "r");
        if (ftag == NULL)
          {
           printf("Warning - cannot open tag file: %s\n", argv[5]);
           printf("          tag option ignored\n"); tag_flag = 0;
          }
        else sxf_rtag();
       }
    }
  doom_open(argv[1]);
  doom_snoup(&snoup);
  sxf_init();
  sxf_out();
  printf("SXF_ex terminated - total number of elements: %d\n", elem_cnt);
  printf("              elements with alignment errors: %d\n", align_cnt);
  printf("              elements with field     errors: %d\n", field_cnt);
  doom_close();
}

char* bpad(char* s, int l)
{
  int j;
  strcpy(pad, s);
  for (j = strlen(s); j < l; j++)  pad[j] = ' ';
  pad[l] = '\0';
  return pad;
}

void sxf_init()
{
  indent = 0; b_level = 0;
}

void sxf_out ()  /* steering routine */
{
  int j, key_list[] = {1, SEQU_CODE};

#ifdef _call_tree_
       puts("+++++++ sxf_out");
#endif
  make_key(sequ_name, key_list, key);
  if (db_exists(key)) pseq = get_obj(key);
  else
    {
     printf("+=+=+= SXF_ex fatal - object %s not in d.b.\n", sequ_name);
     exit(1);
    }
  write_header("// SXF version 1.0");
  sequ_no_elem = pseq->c_int;
  for (j = 0; j < sequ_no_elem; j++)
    {
     if (j == 0) sequ_start = pseq->a_dble[j];
     if (strchr(pseq->names[j], '$') == NULL)  pro_elem(j);
     else   sequ_end = pseq->a_dble[j];
    }
  sequ_length = sequ_end - sequ_start;
  write_trailer(sequ_length);
  if (b_level > 0)
     printf("+=+=+= SXF_ex warning - %d missing '}'\n", b_level);
}

void sxf_rtag()
{
  char *p, *eofp;
  char text[100];
  int j;

  do
    {
     if ((eofp = fgets(text, 100, ftag)) != NULL)
       {
        if ((p = strtok(text, " ,\n")) != NULL)
	  {
           strcpy(tag_type[tag_cnt], p);
           if ((p = strtok(NULL, " ,\n")) != NULL) 
              strcpy(tag_code[tag_cnt++], p);
	  }
       }
    } while (eofp != NULL);
  for (j = 0; j < tag_cnt; j++)
    {
     lower(tag_type[j]); lower(tag_code[j]); 
    }
  if (tag_cnt > 0)  tag_flag = 2;
}

void pro_elem             /* processes one element */
              (int elnum)
{
  int j, bits, key_list[] = {1, ELPAR_CODE};
  char *name;
  char key[KEY_LENGTH];
  double corr[2];

#ifdef _call_tree_
       puts("+++++++ pro_elem");
#endif
  name = pseq->names[elnum];
  if (!db_exists(name))
     printf("+=+=+= SXF_ex warning - element %s not in d.b.\n", name);
  else
    {
     pelem = get_obj(name);
     lower(pelem->base_name);
     lower(pelem->par_name);
     make_key(pelem->base_name, key_list, key);
     if (!db_exists(key))
        printf("+=+=+= SXF_ex warning - element %s base %s not in list\n", 
               name, pelem->base_name);
     else
       {
	doom_gcorrect(name, &pseq->a_int[elnum], &bits, corr);/* corector ? */ 
        if (bits >= 0)
	  {
           for (j = 0; j < 2; j++) pelem->a_dble[j+12] += corr[j];
	  }
        pbase = get_obj(key);
        write_elstart(elnum);  
        write_body(elnum); elem_cnt++;
        nf_err = FIELD_MAX;
        doom_gfield(name, &pseq->a_int[elnum], &nf_err, fd_errors);
        if (nf_err > 0) {write_field(); field_cnt++;}
        na_err = ALIGN_MAX;
        doom_galign(name, &pseq->a_int[elnum], &na_err, al_errors);
        if (na_err > 0) {write_align(); align_cnt++;}
        write_elend();
       }
    }
}

void fill_dump(int flag, char* label, double* values, int count, 
               int inc, double factor)
{
  int j;
  double temp;

  if (flag == 0) sprintf(aux, " %s = ", label);
  else           sprintf(aux, " %s = [", label);
  accu_line(aux);
  for (j = 0; j < count; j += inc)
    {
     if (flag == 2 && factor != zero)  temp = factor * values[j];
     else                              temp = values[j];
     sprintf(aux, " %.12g", temp); accu_line(aux);
    }
  if (flag != 0) 
    {
     accu_line("]"); reset_line();
    }
}

void lower(char* s)
{
  char* cp = s;
  while(*cp != '\0') *cp++ = (char) tolower((int)*cp);
}

void write_body(elnum)
{
  int i, j, k, flag, last, set = 0;
  double factor;

  for (j = 1; j < pbase->c_int; j++)
    {
     factor = zero;
     flag = pbase->a_int[j] / 1000; k = pbase->a_int[j] % 1000;
     if (flag == 0)
       {
        if (k < pelem->c_dble && pelem->a_dble[k] != zero)
          {
           if (set++ == 0) 
	     {
              put_line("body = {"); s_indent(add_indent[4]);
	     }
           fill_dump(flag, pbase->names[j], &pelem->a_dble[k], 1, 1, factor);
	  }
       }
     else
       {
        if (flag == 2)  factor = pelem->a_dble[0];
        last = -1;
        for (i = k; i < pelem->c_dble; i += 2) 
           if (pelem->a_dble[i] != zero) last = i;
        if (last >= 0)
	  {
           last += 1 - k;
           if (set++ == 0) 
	     {
              put_line("body = {");  s_indent(add_indent[4]);
	     }
           fill_dump(flag, pbase->names[j], &pelem->a_dble[k], 
           last, 2, factor);
	  }
       }
    }
  if(set > 0) 
    {
     put_line("}"); r_indent(); 
    }
}

int all_blank(char* s)
{
  int i;

  for (i = 0; i < strlen(s); i++)  if(s[i] != ' ') return 0;
  return 1;
}

void r_indent()
{
  if (b_level == 0)
    {
     printf("+=+=+= SXF_ex fatal - too many closing '}'\n");
     exit(1);
    }
  indent = b_indent[--b_level];
}

void accu_line(char* s)
{
  if (strlen(line) + strlen(s) + indent > LINE_MAX)  reset_line();
  strcpy(&line[strlen(line)], s);
}

void reset_line()
{
  if (all_blank(line) == 0)  put_line(line);
  line[0] = '\0';
}

void put_line(char* s)
{
  char tline[2*LINE_MAX];
  int i;
  if (s != line) reset_line();
  for (i = 0; i < indent; i++) tline[i] = ' ';
  strcpy(&tline[indent], s);
  fprintf(out, "%s\n",tline);
}

void s_indent(int i)
{
  b_indent[b_level] = indent;
  indent += i;
  b_level++;
}

char* tag_spec(char* intype)
{
  int j;

  for (j = 0; j < tag_cnt; j++)
    {
     if (strcmp(intype, tag_type[j]) == 0)  return tag_code[j];
    }
  return NULL;
}

void write_align()
{
  int j, k = -1;

  for (j = 0; j < na_err; j++)  if (al_errors[j] != zero)  k = j;
  if (++k > 0)
    {
     put_line("align.dev = {");  s_indent(add_indent[4]);
     fill_dump(1, "al", al_errors, k, 1, zero);
     put_line("}"); r_indent();
    }     
}

void write_elend()
{
  put_line("};"); 
  r_indent(); r_indent(); r_indent();
}

void write_elstart(int elnum)
{
  int j, k, occnt = pseq->a_int[elnum], flag = pbase->a_int[0] / 1000;
  double temp, ratio;
  char out_name[100];
  char *pc;

  s_indent(add_indent[1]);
  strcpy(out_name, pelem->key);
  if (occnt > 1)  /* add count to name, print warning */
    {
     if (occnt_add++ == 0)
     printf("+=+=+= SXF_ex warning - making names unique\n\n");
     /*     printf("       %s", out_name);  */
     k = strlen(out_name);
     out_name[k++] = '.';
     sprintf(&out_name[k],"%d", occnt);
     /*     printf(" ---> %s\n", out_name);  */
    }
  put_line(out_name); s_indent(add_indent[2]);
  sprintf(aux, "%s {", pelem->base_name); put_line(aux);
  s_indent(add_indent[3]);
  if (tag_flag == 1 && strcmp(pelem->base_name, pelem->par_name) != 0)
    {
     sprintf(aux, "tag = %s", pelem->par_name); 
     put_line(aux);
    }
  else if (tag_flag == 2 && (pc = tag_spec(pelem->base_name)) != NULL)
    {
     sprintf(aux, "tag = %s", pc); 
     put_line(aux);
    }
  if ((temp = pelem->a_dble[0]) > zero)
      {
       if (pelem->a_dble[ANG_POS] != zero && flag != 0)
	 {
          ratio = pelem->a_dble[ANG_POS] / 2; ratio = ratio / sin(ratio);
          temp = pelem->a_dble[0] * ratio;
          for (j = K0_POS; j < K0_POS+10; j++)
             pelem->a_dble[j] = ratio * pelem->a_dble[j];
	 }
       sprintf(aux, "%s = %.12g", pbase->names[0], temp);
       put_line(aux);
      }
  sprintf(aux, "at = %.12g", pseq->a_dble[elnum]);
  put_line(aux);
}

void write_field()
{
  int j, k = -1;

  for (j = 0; j < nf_err; j++)  if (fd_errors[j] != zero)  k = j;
  if (++k > 0)
    {
     put_line("body.dev = {");  s_indent(add_indent[4]);
     fill_dump(1, "kl ", fd_errors, k, 2, zero);
     fill_dump(1, "kls", &fd_errors[1], k, 2, zero);
     put_line("}"); r_indent(); 
    }     
}

void write_header(char* version)
{
  put_line(version);
  sprintf(aux, "%s sequence", sequ_name); put_line(aux);
  s_indent(add_indent[0]); put_line("{");
}

void write_trailer(double sl)
{
  sprintf(aux, "endsequence at = %.12g", sl);
  put_line(aux);
  r_indent(); put_line("}");
  put_line("// SXF end");
}
