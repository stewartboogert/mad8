/* extract Sixtrack input files from DOOM */
/* #define _call_tree_ */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#ifdef _LINUX_
#include "doomex.h"
#include "doom.h"
#include "c6t.h"
#endif
#ifndef _LINUX_
#include "doomex.h"
#include "doom.h"
#include "c6t.h"
#endif

/*---------------------------------------------------------------------*
*                                                                      *
*                           CERN                                       *
*                                                                      *
*     European Organization for Nuclear Research                       *
*                                                                      *
*     Program name: c6t: MAD-Sixtrack Format Converter                 *
*                                                                      *
*     Author and contact:   Hans GROTE                                 *
*                           SL Division                                *
*                           CERN                                       *
*                           CH-1211 GENEVA 23                          *
*                           SWITZERLAND                                *
*                      Tel. [041] (022) 767 49 61                      *
*                           Hans.Grote@cern.ch                         *
*                                                                      *
*     Copyright  CERN,  Geneva  2000  -  Copyright  and  any   other   *
*     appropriate  legal  protection  of  this  computer program and   *
*     associated documentation reserved  in  all  countries  of  the   *
*     world.                                                           *
*                                                                      *
*     Organizations collaborating with CERN may receive this program   *
*     and documentation freely and without charge.                     *
*                                                                      *
*     CERN undertakes no obligation  for  the  maintenance  of  this   *
*     program,  nor responsibility for its correctness,  and accepts   *
*     no liability whatsoever resulting from its use.                  *
*                                                                      *
*     Program  and documentation are provided solely for the use  of   *
*     the organization to which they are distributed.                  *
*                                                                      *
*     This program  may  not  be  copied  or  otherwise  distributed   *
*     without  permission. This message must be retained on this and   *
*     any other authorized copies.                                     *
*                                                                      *
*     The material cannot be sold. CERN should be  given  credit  in   *
*     all references.                                                  *
*                                                                      *
*---------------------------------------------------------------------*/

#define ALIGN_MAX 6         /* alignment error array length */
#define FIELD_MAX 40        /* field error array length */
#define MM_KEEP 2           /* no. of element name starts to keep */
#define N_TYPES 26          /* no. of valid element types */
#define MULTI_MAX 24        /* element array length for multipoles */
#define N34 6               /* no. of values in special fort.34 column */
#define NT34 5              /* no. of element types in special fort.34 */
#define LINES_MAX 3         /* structure output line max. names */
#define SEQ_DUMP_LEVEL 0    /* chooses amount of dumped output */

void add_drifts();
void add_split_list(struct element*);
void add_to_ellist(struct element*);
void app_factor(double, double*, int);
void arr_print(double*, int);
void assign_att();
void att_beambeam(struct element*);
void att_colli(struct element*);
void att_decapole(struct element*);
void att_drift(struct element*);
int f34_values(struct element*, int*, double*);
void att_hkicker(struct element*);
void att_kicker(struct element*);
void att_lcavity(struct element*);
void att_marker(struct element*);
void att_matrix(struct element*);
void att_multipole(struct element*);
void att_octupole(struct element*);
void att_quadrupole(struct element*);
void att_rbend(struct element*);
void att_rfcavity(struct element*);
void att_sbend(struct element*);
void att_sextupole(struct element*);
void att_vkicker(struct element*);
void att_undefined(struct element*);
int c_list_pos(char*, char**, int*, int);
void concat_drifts();
void conv_elem();
void c6t_finish();
void c6t_init();
void dump_element(struct element*);
void dump_sequ(int);
void dump_types(int);
void equiv_elem();
struct block* get_block_equiv(struct block*);
void get_args(int, char**);
void get_error_refs(struct element*);
int get_flag(struct element*, struct type_info*);
struct element* get_from_ellist(char*, char*);
void get_multi_refs();
int get_next_name(char*, char);
void gnu_file(struct element*);
void grow_ellist(struct el_list*);
int ident_el(struct element*, struct element*);
int ident_zero(struct element*);
int in_keep_list(struct element*);
void invert_normal(int, double*);
void invert_skew(int, double*);
void link_behind(struct element*, struct element*);
void link_in_front(struct element*, struct element*);
void link_twiss();
void lower(char*);
void make_element(struct object*);
void make_multipole(struct element*);
void make_mult_ref_list();
void mod_errors();
void mod_lcavity(struct object*);
void mod_multipole(struct object*);
void mod_octupole(struct object*);
void mod_quadrupole(struct object*);
void mod_rbend(struct object*);
void mod_rfcavity(struct object*);
void mod_sextupole(struct object*);
void multi_loop();
struct element* new_element(int, char*, char*);
struct block* new_block();
void post_multipoles();
double power_of(double, int);
void pre_multipole(struct element*);
void pro_elem(int, double);
void process();
void read_aux();
void read_sequ();
void read_twiss();
void remove_from_ellist(struct element*);
void replace(struct element*, struct element*);
int s_list_pos(char*, char**, int);
void split();
void split_kicker(struct element*);
void split_other(struct element*);
void split_special(struct element*);
void supp_elem();
void supp_small_comp(struct object*);
void treat_split(struct element*);
void yank(struct element*);
void write_all_el();
void write_blocks();
void write_element(struct element*);
void write_f16_errors();
void write_f8_errors();
void write_f3aux();
void write_f3_entry(char*, struct element*);
void write_f3_mult(struct element*);
void write_f34_special();
void write_struct();

struct li_list types;

struct type_info* t_info[N_TYPES];

struct block   *first_block, *last_block;
struct block*   prev_block;
struct block*   current_block = NULL;

struct element *first_in_sequ, *last_in_sequ;
struct element* prev_element;
struct element* current_element = NULL;
struct element* debug_element = NULL;
struct el_list* split_list = NULL;

struct object *pseq,        /* pointer to SEQUENCE object */
              *pelem,       /* pointer to current ELEMENT object */
              *pmultref,    /* pointer to multipole reference */
              *p_beam,      /* pointer to beam object */
              *p_err_zero,  /* pointer to error object with all zeroes */
              *p_twhead,    /* pointer to twiss table header */
              *p_twiss,     /* pointer to twiss table */
              *pbase;       /* pointer to current BASE object */

struct ref_list multipole_reflist;

char          sequ_name[KEY_LENGTH],
              doom_file[120],
              key[KEY_LENGTH];

char el_info[N_TYPES][60] = /* see type_info definition */
/*           l=0 l>0,normal l>0,skew ->drift make_k*l split */
{"beambeam    2       2       2       0       0       0",
"beamint      0       1       1       1       0       0",
"drift        0       1       1       0       0       0",
"decapole     2       2       2       0       1       2", 
"ecollimator  0       1       1       0       0       0",
"elseparator  0       1       1       1       0       0",
"gbend        1       1       1       2       1       1",
"hkicker      5       5       5       1       0       3",
"hmonitor     0       1       1       1       0       0",
"instrument   0       1       1       1       0       0",
"kicker       6       6       6       1       0       3",
"lcavity      3       3       3       0       0       2",
"marker       4       0       0       0       0       0",
"matrix       2       2       2       0       0       0",
"monitor      0       1       1       1       0       0",
"multipole    2       2       2       0       0       0",
"octupole     2       2       2       0       1       2",
"quadrupole   2       1       2       0       1       1",
"rbend        2       1       1       0       1       1",
"rcollimator  0       1       1       0       0       0",
"rfcavity     3       3       3       0       0       2",
"sbend        2       1       1       0       1       1",
"sextupole    2       2       2       0       1       2",
"solenoid     0       1       1       2       1       0",
"vkicker      5       5       5       1       0       3",
"vmonitor     0       1       1       1       0       0"};

char keep_these[MM_KEEP][24] = {"ip", "mt_"};
char mpole_names[][16] = {"dipole", "quadrupole", "sextupole",
                          "octupole", "decapole", "multipole"};
char acro_list[20];   /* list for name starts */
int acro_cnt[20];    /* counters for name starts */
char tmp_name[KEY_LENGTH];

  int s_pos, pos34[N34];
  char out_vals[N34][8] = {"BETX", "BETY", "MUX", "MUY", "X", "Y"};

int           sequ_no_elem,        /* no. of elements in sequence */
              block_count = 0,     /* current block count for naming */
              elem_cnt = 0,        /* element count */
              acro_occ = 0,        /* acro list occupation */
              align_cnt = 0,       /* element with align errors count */
              field_cnt = 0,       /* element with field errors count */
              f3_cnt = 0,          /* f3 write flag */
              f3aux_cnt = 0,       /* f3aux write flag */
              f8_cnt = 0,          /* f8 write count */
              f16_cnt = 0,         /* f16 write count */
              f34_cnt = 0,         /* f34 write count */
              special_flag = 1,    /* produce special output file from twiss */
              cavall_flag = 0,     /* if 0 lump all cavities into first */
              split_flag = 0,      /* if 1 keep zero multipoles after split */
              multi_type = -1,     /* is set to multipole type if any found */
              twiss_rows = 0,      /* no. of rows in Twiss table */
              cavity_count = 0;    /* count cavities in output */

double        sequ_length,         /* length of  sequence */
              sequ_start, 
              sequ_end,
              total_voltage = 0,
              harmon = 0,
              aux_val[4] = {-1.e20, -1.e20, -1.e20, -1.e20},
              error_matrix[FIELD_MAX],
              tmp_buff[FIELD_MAX];

const double zero  = 0;
const double two   = 2;
const double ten   = 10;
const double c1p3 = 1.e3;
const double eps_6 = 1.e-6;
const double eps_9 = 1.e-9;
const double eps_12 = 1.e-12;
double ref_def = 0.017;

FILE *f2, *f3, *f3aux, *f8, *f16, *f34;

int main(int argc, char *argv[])  /* writes the Sixtrack input files
                                      extracted from a DOOM database */
{
  int snoup = 1;

  if (argc < 2) 
    {
     puts("c6t usage: c6t dbname options");
     exit(1);
    }
  get_args(argc, argv);
  doom_open(doom_file);
  doom_snoup(&snoup);
  c6t_init();
  process();
  printf("\nc6t terminated - total number of elements: %d\n", elem_cnt);
  printf("                    with alignment errors: %d\n", align_cnt);
  printf("                    with field     errors: %d\n", field_cnt);
  printf("                          sequence length: %f [m]\n", sequ_length);
  doom_close();
  c6t_finish();
  return 0;
}

void add_drifts()
{
  int af;
  struct element *d1;
  double pos = sequ_start, dl, el2;
  char c[24];
  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     el2 = current_element->value[0] / two;
     dl = current_element->position - el2 - pos;
     if (dl + eps_9 < zero)
       {
        printf(
        "+=+=+= c6t fatal - negative drift in front of %s, length %f\n",
        current_element->name, dl);
        exit(1);
       }
     else if (dl > eps_9)
       {
        af = get_next_name(c, 'd');
        d1 = new_element(1, c, "drift");
        d1->value[0] = dl; d1->flag = 1;
        link_in_front(d1, current_element);
        d1->position = pos + dl / two;
        if (af != 0)  add_to_ellist(d1);
       } 
     pos = current_element->position + el2;
     current_element = current_element->next;
    }
}

void add_split_list(struct element* el)
{
  int i;
  if (split_list == NULL) 
    {
     split_list = (struct el_list*) calloc(1, sizeof(struct el_list));
     split_list->elem = 
       (struct element**) calloc(EL_COUNT, sizeof(struct elem*));
     split_list->max = EL_COUNT;
    }
  else if (split_list->curr == split_list->max) grow_ellist(split_list);
  for (i = 0; i < split_list->curr; i++) if (split_list->elem[i] == el) return;
  split_list->elem[split_list->curr++] = el;
}

void add_to_ellist( /* adds element to correct object list */
		 struct element* p_elem)
{
  int j;

#ifdef _call_tree_
  puts("+++++++ add_to_ellist");
#endif
  for (j = 0; j < types.curr; j++)
    {
     if (strcmp(types.member[j]->base_name, p_elem->base_name) == 0)
       {
        if (types.member[j]->curr == types.member[j]->max) 
           grow_ellist(types.member[j]);
        types.member[j]->elem[types.member[j]->curr++] = p_elem;
        return;
       }
    }
  /* type list does not exist - create */
  if (types.curr == BASE_TYPES)
    {
     printf("+++ fatal - %s overruns type buffer of %d types\n",
	    p_elem->base_name, types.curr);
     exit(1);
    }
  types.member[types.curr] 
       = (struct el_list*) calloc(1,sizeof(struct el_list));
  types.member[types.curr]->elem
       = (struct element**) calloc(EL_COUNT, sizeof(struct element*));
  types.member[types.curr]->elem[types.member[types.curr]->curr++] = p_elem;
  types.member[types.curr]->max = EL_COUNT;
  strcpy(types.member[types.curr]->base_name, p_elem->base_name);
  types.curr++;
}

void app_factor(double fact, double* array, int count)
{
  int i;
  for (i = 0; i < count; i++) array[i] *= fact;
}

void arr_print(double array[], int occ)
{
  int i;
  for (i = 0; i < occ; i++)
    {
     printf(" %12.4e", array[i]); if ((i+1)%5 == 0) printf("\n");
    }
  printf("\n");
}

void assign_att()
{
  struct element *el;
  int i, j;
  
  for (i = 0; i < types.curr; i++)  /* loop over base types */
   {
    for (j = 0; j < types.member[i]->curr; j++) /* loop over el. in type */
     {
      el = types.member[i]->elem[j];
      if (el->flag > 0 && el->equiv == el)  /* all others ignored */
       {
    	 if (strcmp(el->base_name, "beambeam") == 0) att_beambeam(el);
    	 else if (strcmp(el->base_name, "decapole") == 0) att_decapole(el);
    	 else if (strcmp(el->base_name, "drift") == 0) att_drift(el);
    	 else if (strcmp(el->base_name, "ecollimator") == 0) att_colli(el);
    	 else if (strcmp(el->base_name, "hkicker") == 0) att_hkicker(el);
    	 else if (strcmp(el->base_name, "kicker") == 0) att_kicker(el);
    	 else if (strcmp(el->base_name, "lcavity") == 0) att_lcavity(el);
    	 else if (strcmp(el->base_name, "marker") == 0) att_marker(el);
    	 else if (strcmp(el->base_name, "matrix") == 0) att_matrix(el);
    	 else if (strcmp(el->base_name, "multipole") == 0) att_multipole(el);
    	 else if (strcmp(el->base_name, "octupole") == 0) att_octupole(el);
    	 else if (strcmp(el->base_name, "quadrupole") == 0) att_quadrupole(el);
    	 else if (strcmp(el->base_name, "rbend") == 0) att_rbend(el);
    	 else if (strcmp(el->base_name, "rcollimator") == 0) att_colli(el);
    	 else if (strcmp(el->base_name, "rfcavity") == 0) att_rfcavity(el);
    	 else if (strcmp(el->base_name, "sbend") == 0) att_sbend(el);
    	 else if (strcmp(el->base_name, "sextupole") == 0) att_sextupole(el);
    	 else if (strcmp(el->base_name, "vkicker") == 0) att_vkicker(el);
    	 else att_undefined(el);
       }
     }
   }
}

void att_beambeam(struct element* el)
{
  int add;
  add = el->twtab_row * p_twhead->c_obj;
  el->out_1 = 20; 
  el->out_2 = c1p3*(el->value[12] - p_twiss->a_dble[add+pos34[4]]);
  el->out_3 = c1p3*(el->value[13] - p_twiss->a_dble[add+pos34[5]]);
  el->out_4 = el->value[16];
}

void att_colli(struct element* el)
     /* ecollim. + rcollim. - make drift, do not concatenate */
{
  el->out_1 = 0; el->out_4 = el->value[0];
}

void att_decapole(struct element* el)
{
  if (el->value[20] != zero)
    {
     el->out_1 = 5; el->out_2 = -el->value[20]/24;
    }
  else if (el->value[21] != zero)
    {
     el->out_1 = -5; el->out_2 = el->value[21]/24;
    }
  else el->out_1 = 0;
}

void att_drift(struct element* el)
{
  el->out_4 = el->value[0];
}

void att_hkicker(struct element* el)
{
 el->out_1 = 1; el->out_2 = el->value[12];
}

void att_kicker(struct element* el)
{
}

void att_lcavity(struct element* el)
{
  double lag = -el->value[5];
  el->out_1 = 12;
  el->out_2 = cavall_flag == 0 ? total_voltage : el->value[1];
  el->out_3 = harmon = p_beam->a_dble[41] / el->value[11];
  printf("harmon: %e\n", harmon);
  if (lag < -0.5) lag +=1.;
  else if (lag > 0.5) lag -=1.;
  el->out_4 = 360. * lag;
}

void att_marker(struct element* el)
{
}

void att_matrix(struct element* el)
{
}

void att_multipole(struct element* el)
{
  el->out_1 = 11;
  if (el->nc_pos == 0)  
    {
     el->out_2 = el->out_3 = 1;
    }
  else 
    {
     el->out_3 = el->rad_length;
     if (el->nc_pos == 12)
       {
	el->out_2 = -el->value[12]; el->out_4 = -1;
       }
     else if (el->nc_pos == 13)
       {
	el->out_2 = el->value[13]; el->out_4 = -2;
       }
    }
}

void att_octupole(struct element* el)
{
  if (el->value[18] != zero)
    {
     el->out_1 = 4; el->out_2 = -el->value[18]/6;
    }
  else if (el->value[19] != zero)
    {
     el->out_1 = -4; el->out_2 = el->value[19]/6;
    }
  else el->out_1 = 0;
}

void att_quadrupole(struct element* el)
{
  el->out_4 = el->value[0];
  if (el->value[14] != zero)
    {
      el->out_1 = 2;
      if (el->value[0] == zero) el->out_2 = -el->value[14];
      else                      el->out_3 = -el->value[14];
    }
  else if (el->value[15] != zero)
    {
     el->out_1 = -2; el->out_2 = el->value[15];
    }
  else el->out_1 = 0;
}

void att_rbend(struct element* el)
{
  el->out_4 = el->value[0];
  if (el->value[12] != zero)
    {
     el->out_2 = -el->value[1];
     if (el->value[14] == zero)  el->out_1 = 1;
     else
       {
        el->out_1 = 6;
        el->out_3 = -el->value[14];
       }
    }
  else if (el->value[13] != zero)
    {
     el->out_2 = el->value[1];
     if (el->value[15] == zero)  el->out_1 = 4;
     else
       {
        el->out_1 = 4;
        el->out_3 = el->value[15];
       }
    }
  else el->out_1 = 0;
}

void att_rfcavity(struct element* el)
{
  double lag = 0.5 - el->value[5];
  el->out_1 = 12;
  if (cavall_flag == 0)
    {
     el->out_2 = total_voltage;
     strcpy(el->name, "CAV");
    }
  else el->out_2 = el->value[1];
  el->out_3 = harmon = el->value[11];
  if (lag < -0.5) lag +=1.;
  else if (lag > 0.5) lag -=1.;
  el->out_4 = 360. * lag;
}

void att_sbend(struct element* el)
{
  el->out_4 = el->value[0];
  if (el->value[12] != zero)
    {
     el->out_2 = -el->value[1];
     if (el->value[14] == zero)  el->out_1 = 3;
     else
       {
        el->out_1 = 6;
        el->out_3 = -el->value[14];
       }
    }
  else if (el->value[13] != zero)
    {
     el->out_1 = 5;
     el->out_2 = el->value[1];
     el->out_3 = el->value[15];
    }
  else el->out_1 = 0;
}

void att_sextupole(struct element* el)
{
  if (el->value[16] != zero)
    {
     el->out_1 = 3; el->out_2 = -el->value[16]/two;
    }
  else if (el->value[17] != zero)
    {
     el->out_1 = -3; el->out_2 = el->value[17]/two;
    }
  else el->out_1 = 0;
}

void att_vkicker(struct element* el)
{
 el->out_1 = -1; el->out_2 = el->value[13];
}

void att_undefined(struct element* el)
{
  el->out_4 = el->value[0];
}

void block_it()
{
  struct element* el;

  current_element = first_in_sequ;
  while ((el = current_element) != NULL)
    {
     current_block = new_block();
     current_block->previous = prev_block;
     current_block->next = NULL;
     if (prev_block == NULL) first_block = current_block;
     else                    prev_block->next = current_block;
     current_block->elements
       = (struct el_list*) calloc(1,sizeof(struct el_list));
     current_block->elements->elem
       = (struct element**) calloc(EL_COUNT, sizeof(struct element*));
     current_block->elements->max = EL_COUNT;
     current_block->first = el;
     current_block->length = el->equiv->value[0];
     current_block->elements->elem[0] = el; 
     current_block->elements->curr = 1;
     if (el->flag < 2)
       {
	while (el->next != NULL && el->next->flag < 2)
          {
           el = el->next;
           current_block->length += el->equiv->value[0];
           if (current_block->elements->curr == current_block->elements->max)
	     grow_ellist(current_block->elements);
           current_block->elements->elem[current_block->elements->curr++] 
           = el;
          }
	current_element = el;
       }
     current_block->last = current_element;
     if (current_block->first == current_block->last &&
         current_block->last->flag >= 2)  current_block->flag = 0;
     else current_block->flag = 1;
     current_block->equiv = get_block_equiv(current_block);
     current_element = current_element->next;
     prev_block = current_block;
    }
   last_block = current_block;
}

int c_list_pos(char* p, char** vlist, int* index, int length)
{
  int num, mid, low = 0, high = length - 1;
  while (low <= high)
    {
     mid = (low + high) / 2;
     if ((num=strcmp(p, vlist[index[mid]])) < 0)  high = mid - 1;
     else if ( num > 0)                    low  = mid + 1;
     else                                  return index[mid];
    }
    return -1;
}

void concat_drifts()
{
  struct element *d1, *temp, *nk;
  int flag, cnt;
  double suml, pos;
  char c[24];
  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     cnt = 0; 
     suml = current_element->value[0];
     pos = current_element->position - suml / two;
     if (strcmp(current_element->base_name, "drift") == 0)
       {
	temp = current_element->next;
	while (temp != NULL && strcmp(temp->base_name, "drift") == 0)
	  {
	   suml += temp->value[0]; cnt++;
           temp = temp->next;
	  }
       }
     if (cnt > 0) /* actually concatenated something */
       {
	flag = get_next_name(c, 'd');
        d1 = new_element(1, c, "drift");  d1->flag = 1;
        d1->value[0] = suml; d1->position = pos + suml / two;
        if (flag != 0) add_to_ellist(d1);
	temp = current_element->next;
	while (temp != NULL && strcmp(temp->base_name, "drift") == 0)
	  {
           nk = temp->next;
           yank(temp);
           temp = nk;
	  }
        if (current_element == first_in_sequ) first_in_sequ = d1;
        replace(current_element, d1); current_element = d1;
       }
     current_element = current_element->next;
    }
}

void conv_elem()
{
  int i, j, nup;
  struct type_info* type = NULL;
  struct element* el;

  for (i = 0; i < types.curr; i++)  /* loop over base types */
    {
      for (j = 0; j < N_TYPES; j++)
	{
	 if (strcmp(types.member[i]->base_name, t_info[j]->name) == 0)
	   {
	    type = t_info[j]; break;
	   }
	}
      if (type == NULL)
        {
         printf("+=+=+= c6t fatal - type %s not defined\n",
                types.member[i]->base_name);
         exit(1);
        }
      nup = types.member[i]->curr;
      for (j = 0; j < nup; j++) /* loop over el. in type */
	{
	 el = types.member[i]->elem[j];
         if (type->flag_4 > 1)
	   printf("+++ warning - treated as drift: %s\n", el->name);
         el->flag = get_flag(el, type);
         if (el->flag > 0)  /* all others ignored */
	   {
	    if (el->value[0] < eps_9)  
	      {
               el->value[0] = zero;
               if (el->flag == 1)  el->flag = 0;
	      }
            if (el->flag > 0)
	      {
               el->c_drift = type->flag_4;
               el->force = type->flag_5; 
               el->split = type->flag_6;
               if (el->split > 0) add_split_list(el);
	      }
	   }
	}
    }
}

void c6t_finish()
{
}

void c6t_init()
{
  struct object* p;
  int j;
  char key[KEY_LENGTH];
  for (j = 0; j < N_TYPES; j++)
    {
     t_info[j] = (struct type_info*) malloc(sizeof(struct type_info));
     sscanf(el_info[j],"%s%d%d%d%d%d%d",t_info[j]->name, &t_info[j]->flag_1,
            &t_info[j]->flag_2, &t_info[j]->flag_3, &t_info[j]->flag_4,
            &t_info[j]->flag_5, &t_info[j]->flag_6);
    }
  p_err_zero = make_obj("zero_errors", 0, FIELD_MAX, 0, 0);
  /* get multipole error reference list if any */
  if ((pmultref = doom_fetch("MULTREFS_MAD")) != NULL) make_mult_ref_list();
  if ((p = doom_fetch("UNNAMED_USE")) == NULL)
    {
     printf("+=+=+= c6t fatal - no used sequence in d.b.\n");
     exit(1);
    }
  strcpy(sequ_name, p->a_char);
  sprintf(key, "BEAM.%s\n", sequ_name);
  p_beam = doom_fetch(key);
}

void dump_element(struct element* el)
{
  int j;
  char pname[24] = "NULL", nname[24] = "NULL";
  if (el->previous != NULL) strcpy(pname, el->previous->name);
  if (el->next != NULL) strcpy(nname, el->next->name);
  printf("name: %s  base: %s  position: %f\n", el->name, el->base_name,
         el->position);
  printf("  names of - previous: %s  next: %s  equiv: %s\n",
         pname, nname, el->equiv->name);
  printf("  flag: %d  force: %d  split: %d keep: %d values: %d f_errors: %d\n",
         el->flag, el->force, el->split, el->keep_in,el->n_values, el->nf_err);
  for (j = 0; j < el->n_values; j++)
    {
     printf("%e ", el->value[j]);
     if ((j+1)%5 == 0 || j+1 == el->n_values)  printf("\n");
    }
  if (el->nf_err)  puts("field errors");
  for (j = 0; j < el->nf_err; j++)
    {
     printf("%e ", el->p_fd_err->a_dble[j]);
     if ((j+1)%5 == 0 || j+1 == el->nf_err)  printf("\n");
    }
  if (el->na_err)  puts("alignment errors");
  for (j = 0; j < el->na_err; j++)
    {
     printf("%e ", el->p_al_err->a_dble[j]);
     if ((j+1)%5 == 0 || j+1 == el->na_err)  printf("\n");
    }
}

void dump_sequ(int level)
{
  double suml = zero;
  puts("+++++++++ dump sequence +++++++++");
  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     suml += current_element->value[0];
     if (level > 2)  dump_element(current_element);
     else if (level > 1)  gnu_file(current_element);
     else if (level > 0 && strcmp(current_element->base_name, "drift") != 0)
       printf("%s: %s at = %f\n", current_element->name, 
       current_element->equiv->name, current_element->position);
     current_element = current_element->next;
    }
  printf("=== sum of element length: %f\n", suml);
}

void dump_types(int flag)
{
  int i, j;

  puts("+++++++++ dump types +++++++++");
  for (i = 0; i < types.curr; i++)
    {
     puts(types.member[i]->base_name);
     for (j = 0; j < types.member[i]->curr; j++) 
       printf("       %s  %f\n", types.member[i]->elem[j]->name, 
                                 types.member[i]->elem[j]->value[0]);
     if (flag > 0) dump_element(types.member[i]->elem[j]);
    }
}

void equiv_elem()
{
  int i, j, k;
  struct element *el, *eln;

  for (i = 0; i < types.curr; i++)  /* loop over base types */
    {
     for (j = 0; j < types.member[i]->curr; j++) /* loop over el. in type */
	{
	 el = types.member[i]->elem[j];
         if (el->flag > 0)  /* all others ignored */
	   {
	    if (el->equiv == el /* not yet equivalenced */
            && strcmp(el->base_name,"marker") != 0) /* do not touch markers */
	      {
	       for (k = j+1; k < types.member[i]->curr; k++)
		 {
		  eln = types.member[i]->elem[k];
		  if (eln->flag > 0 
                      && eln->equiv == eln 
                      && ident_el(el, eln) == 0
                      && strcmp(eln->base_name,"marker") != 0)
                    eln->equiv = el;
		 }
	      }
	   }
	}
    }
}

int f34_values(struct element* el, int* flags, double* values)
{
  int i, j, np, nd, cnt = 0;
  double pow, tmp[FIELD_MAX];
  for (i = 0; i < FIELD_MAX; i++)
    {
     tmp[i] = zero;
     j = i + 12;
     if (j < el->n_values && el->value[j] != zero)
       {
	if (el->value[0] != zero) tmp[i] += el->value[0] * el->value[j];
        else tmp[i] += el->value[j];
       }
     if (i < el->nf_err && el->p_fd_err->a_dble[i] != zero)
       tmp[i] += el->p_fd_err->a_dble[i];
    }
  for (i = 3; i < FIELD_MAX; i++)
    {
     if (tmp[i] != zero)
       {
        np = i / 2 + 1;
        nd = 1; for (j = 2; j < np; j++)  nd *= j;
        pow = nd;
        pow = power_of(ten, 6-3*np) / pow;
        if (i%2 == 0)  
	  {
           flags[cnt] = np; if (el->npole_sign) pow = -pow;
	  }
        else           flags[cnt] = -np;
        values[cnt++] = pow * tmp[i];
       }
    }
  return cnt;
}

struct block* get_block_equiv(struct block* current)
{
  struct block* p = first_block;
  int i, k;
  while (p != current)
    {
     if (current->elements->curr == p->elements->curr)
       {
	k = 0;
        for (i = 0; i < current->elements->curr; i++)
	  {
	   if (strcmp(current->elements->elem[i]->equiv->name,
	   p->elements->elem[i]->equiv->name) == 0) k++;
	  }
        if (k == current->elements->curr)  return p;
       }
     p = p->next;
    }
  return p;   
}

void get_args(int argc, char* argv[])
{
  int i;
  double ref_tmp;

  strcpy(doom_file, argv[1]);
  for (i = 2; i < argc; i++)
    {
     if (strcmp(argv[i], "cavall") == 0)  cavall_flag = 1;
     else if (strcmp(argv[i], "split") == 0)   split_flag = 1;
     else if (strcmp(argv[i], "radius") == 0 && i+1 < argc)
       {  
        sscanf(argv[i+1], "%le", &ref_tmp);
        if (ref_tmp > zero) ref_def = ref_tmp;
       }
    }
}

void get_error_refs(struct element* el)
{
  int i;
  char t_name[KEY_LENGTH];
  char* c;
  double tmp;
  if (pmultref == NULL)  el->ref_radius = ref_def;  /* default */
  else
    {
     strcpy(t_name, el->name);
     if ((c = strchr(t_name, '+')) != NULL) *c = '\0'; /*remove occurrence */
     if ((i = c_list_pos(t_name, multipole_reflist.names,
          multipole_reflist.index, multipole_reflist.size)) > -1)
       {
        el->mult_order = multipole_reflist.orders[i];
        el->ref_radius = multipole_reflist.radii[i];
       }
     else 
       {
	printf("+++ warning - not in multref table: %s\n", t_name);
        el->ref_radius = ref_def;  /* default */
       }
    }
  i = 12 + 2 * el->mult_order;
  if (i+1 < el->n_values)
    {
     tmp = fabs(el->value[i]) > fabs(el->value[i+1]) ?
           fabs(el->value[i]) : fabs(el->value[i+1]);
    }
  else if(i < el->n_values) tmp = fabs(el->value[i]);
  else tmp = 1;
  if (tmp == zero) tmp = 1;
  el->ref_delta = c1p3 * tmp * power_of(el->ref_radius, el->mult_order);
}

int get_flag(struct element* el, struct type_info* type)
{

  if (el->value[0] == zero)
    {
     if (type->flag_1 == 4) return in_keep_list(el);
     else return type->flag_1;
    }
  if (el->n_values < 7) return type->flag_2;
  else return (el->value[6] == 0 ? type->flag_2 : type->flag_3);
}

struct element* get_from_ellist(char* name, char* type)
{
  int i, j;

#ifdef _call_tree_
  puts("+++++++ get_from_ellist");
#endif
  for (i = 0; i < types.curr; i++)
   {
    if (strcmp(types.member[i]->base_name, type) == 0)
       {
        for (j = 0; j < types.member[i]->curr; j++) /* loop over el. in type */
          {
	   if (strcmp(types.member[i]->elem[j]->name, name) == 0) 
	     return types.member[i]->elem[j];
          }
       }
    }
  return NULL;
}

void get_multi_refs()
{
  int i;
  for (i = 0; i < types.curr; i++)  /* loop over base types */
    {
     if (strcmp(types.member[i]->base_name, "multipole") == 0) 
       {
	multi_type = i;  break;
       }
    }
}

int get_next_name(char* name, char acro)
{
  char s[2];
  int j, k = -1;

  for (j = 0; j < acro_occ; j++) if (acro_list[j] == acro)  k = j;
  if (k < 0)
    {
     k = acro_occ++; acro_list[k] = acro; acro_cnt[k] = 0;
    }
  s[0] = acro; s[1] = '\0';
  sprintf(name, "%s_c6t_%d", s, ++acro_cnt[k]);
  return 1;
}

void gnu_file(struct element* el)
{
  double el_start, el_end;

  el_start = el->position - el->value[0] / two;
  el_end   = el->position + el->value[0] / two;
  printf("%s %e 0.\n", el->name, el_start);
  printf("%s %e 1.\n", el->name, el_start);
  printf("%s %e 1.\n", el->name, el_end);
  printf("%s %e 0.\n", el->name, el_end);
}

void grow_ellist( /* doubles object list size */
		 struct el_list* p)
{
  struct element** p_loc = p->elem;
  int j, new = 2*p->max;

#ifdef _call_tree_
       puts("+++++++ grow_ellist");
#endif
  p->max = new;
  p->elem = (struct element**) calloc(new, sizeof(struct element*));
  for (j = 0; j < p->curr; j++) p->elem[j] = p_loc[j];
  free(p_loc);
}

int ident_el(struct element* el1, struct element* el2)
{
  double s, tolerance = eps_9;
  int j, m = el1->n_values < el2->n_values ? el1->n_values : el2->n_values;
  if (el1->mult_order != el2->mult_order)  return 1;
  if (el1->ref_radius != el2->ref_radius)  return 2;
  if (el1->ref_delta  != el2->ref_delta)   return 2;
  if (el1->keep_in  != el2->keep_in)       return 6;
  for (j = 0; j < m; j++)
    {
     s = fabs(el1->value[j]) + fabs(el2->value[j]);
     if (s > zero 
         && fabs(el1->value[j] - el2->value[j])/s > tolerance) return 3;
    }
  if (m != el1->n_values)
    {
     for (j = m; j < el1->n_values; j++)
       if (el1->value[j] != zero) return 4;
    }
  else if (m != el2->n_values)
    {
     for (j = m; j < el2->n_values; j++)
       if (el2->value[j] != zero) return 5;
    }
  return 0;
}

int ident_zero(struct element* el)
{
  int j;
  for (j = 12; j < el->n_values; j++)
    if (el->value[j] != zero) return 1;
  return 0;
}

int in_keep_list(struct element* el)
{
  char temp[24];
  int j;

  strcpy(temp, el->name); lower(temp);
  for (j = 0; j < MM_KEEP; j++)
    {
     if (strncmp(temp, keep_these[j], strlen(keep_these[j])) == 0) return 2;
    }
  return 0;
}

void invert_normal(int count, double array[])
{
  int i;
  for (i = 0; i < (count+1)/2; i++)  array[2*i] = -array[2*i];
}

void invert_skew(int count, double array[])
{
  int i;
  for (i = 0; i < count/2; i++)  array[2*i+1] = -array[2*i+1];
}

void link_in_front(struct element* new, struct element* el)
{
  if (el->previous == NULL) first_in_sequ = new;
  else el->previous->next = new;
  new->previous = el->previous; new->next = el;
  el->previous = new;
}

void link_behind(struct element* new, struct element* el)
{
  if (el->next == NULL) last_in_sequ = new;
  else el->next->previous = new;
  new->previous = el; new->next = el->next;
  el->next = new;
}
 
void link_twiss()
{
  int row_cnt = 0;
  
  if (p_twhead == NULL || p_twiss == NULL)  return;
  twiss_rows = p_twhead->a_int[p_twhead->c_int - OPTICS_INTL + 1];

  /* Twiss header object:
     # rows: twiss_rows above
     # columns: p_twhead->c_obj
     # column names: p_twhead->names
     # column name positions: p_twhead->a_int

     Twiss table object:
     # element names: p_twiss->names
     # occurrence counts: p_twiss->a_int
  */

  current_element = first_in_sequ;
  while (current_element != NULL)
    { 
     current_element->twtab_row = -1;
     while(row_cnt < twiss_rows 
     && (strcmp(p_twiss->names[row_cnt], current_element->org_name) != 0
     || (p_twiss->a_int[row_cnt] != current_element->occ_cnt))) row_cnt++;
     if (row_cnt == twiss_rows)
	{
         printf(
         "+=+=+= c6t fatal - sequence element %s (%d) not in Twiss table\n",
         current_element->org_name, current_element->occ_cnt);
         exit(1);
	}
     current_element->twtab_row = row_cnt;
     current_element = current_element->next;
    }
}

void lower(char* s)
{
  char* cp = s;
  while(*cp != '\0') 
    {
     *cp = (char) tolower((int)*cp); cp++;
    }
}

void make_element(struct object* p)
{
  int j;
  prev_element = current_element;
  current_element = new_element(p->c_dble, p->key, p->base_name);
  if (elem_cnt++ == 0) first_in_sequ = current_element;
  else                 prev_element->next = current_element;
  current_element->previous = prev_element;
  current_element->next = NULL;
  for (j = 0; j < p->c_dble; j++) 
     current_element->value[j] 
     = fabs(p->a_dble[j]) > eps_12 ? p->a_dble[j] : zero;
}

void make_multipole(struct element* el)
{
  if (el->force > 0) /* multiply forces with length */
  app_factor(el->value[0], &el->value[12], el->n_values-12);
  el->value[0] = zero; /* set element length to zero */
  el->flag = 2;
  remove_from_ellist(el);
  strcpy(el->base_name, "multipole");
  add_to_ellist(el);
}

void make_mult_ref_list()
{
  int i, j, k = pmultref->c_int, flag = 1;
  char* c = pmultref->a_char;
  multipole_reflist.size = k;
  multipole_reflist.orders = pmultref->a_int;
  multipole_reflist.radii = pmultref->a_dble;
  multipole_reflist.names = (char**) malloc(k * sizeof(char*));
  multipole_reflist.index = (int*) malloc(k * sizeof(int));
  for (j = 0; j < k; j++)  
    {
     multipole_reflist.names[j] = c;
     while (*c != '\0')  c++;
     c++;
    }
  /* sort */
  for (j = 0; j < k; j++)  multipole_reflist.index[j] = j;
  while(flag)
    {
     flag = 0;
     for (j = 1; j < k; j++)
       {
        if (strcmp(multipole_reflist.names[multipole_reflist.index[j-1]],
            multipole_reflist.names[multipole_reflist.index[j]]) > 0)
	  {
           flag = 1;
           i = multipole_reflist.index[j-1];
           multipole_reflist.index[j-1] = multipole_reflist.index[j];
           multipole_reflist.index[j] = i;
	  }
       }
    }
  /*  for (j = 0; j < k; j++)  
      puts(multipole_reflist.names[multipole_reflist.index[j]]); */
}

void mod_errors()
{
  current_element = first_in_sequ;
  while (current_element != NULL)
   {
    if (current_element->nf_err > 0) 
     invert_normal(current_element->nf_err, current_element->p_fd_err->a_dble);
    current_element = current_element->next;
   }
}

void mod_lcavity(struct object* p)
{
  total_voltage += p->a_dble[1];  /* accumulate voltage */
}

void mod_multipole(struct object* p)
{
  supp_small_comp(p);
}

void mod_octupole(struct object* p)
{
  supp_small_comp(p);
}

void mod_quadrupole(struct object* p)
{
  supp_small_comp(p);
}

void mod_rbend(struct object* p)
{
  supp_small_comp(p);
  /* 
 set length to arc length
 if (fabs(p->a_dble[0]) > eps_9 && fabs(p->a_dble[10]) > eps_9)
    p->a_dble[0] *= p->a_dble[10] / (two * sin(p->a_dble[10]/two));
  */
  /* change 1/r to value for straight line */
  p->a_dble[1] = p->a_dble[10] / p->a_dble[0];
}

void mod_rfcavity(struct object* p)
{
  total_voltage += p->a_dble[1];  /* accumulate voltage */
}

void mod_sextupole(struct object* p)
{
  supp_small_comp(p);
  /* supress tilt angle (not component !) */
  p->a_dble[6] = zero;
}

void multi_loop()
{
  int i, j, nup;
  struct element* el;
  for (i = 0; i < types.curr; i++)  /* loop over base types */
    {
     if (strcmp(types.member[i]->base_name, "multipole") == 0)
       {
        nup = types.member[i]->curr;
        for (j = 0; j < nup; j++) /* loop over mutipoles */
	  {
	   el = types.member[i]->elem[j];
           pre_multipole(el);
	  }
       }
    }
}

struct block* new_block()
{
  struct block* p;
  p = (struct block*) calloc(1, sizeof(struct block));
  sprintf(p->name, "BLOC%d", block_count++);
  return p;
}

struct element* new_element(int size, char* name, char* base)
{
  struct element* p;
  p = (struct element*) calloc(1, sizeof(struct element));
  strcpy(p->name, name);
  p->equiv = p;
  strcpy(p->base_name, base);
  p->value = (double*) calloc(++size,sizeof(double));
  p->n_values = size;
  return p;
}

void post_multipoles() /* post equiv. treatment of multipoles */
{
  int i, j;
  struct element *el, *eln;
  struct object *p;

  if (multi_type > -1) /* there are multipoles */
    {
     if (pmultref == NULL)
        printf("+++ warning: no multipole reference table in database\n");
     for (j = 0; j < types.member[multi_type]->curr; j++)
	{
	 el = types.member[multi_type]->elem[j]; eln = el->equiv;
         if (el->nf_err > 0)
	   {
            eln->mult_order = el->mult_order;
            eln->ref_radius = el->ref_radius;
            if (eln->p_fd_err == NULL) 
	      {
               eln->p_fd_err = p_err_zero;
               eln->nf_err = FIELD_MAX;
	      }
            if (eln->nf_err < el->nf_err) 
	      {
               strcpy(tmp_name, eln->p_fd_err->key);
               p = eln->p_fd_err;
               eln->p_fd_err = make_obj(tmp_name, 0, el->nf_err, 0, 0);
               for (i = 0; i < eln->nf_err; i++) 
		   eln->p_fd_err->a_dble[i] = p->a_dble[i];
               eln->nf_err = el->nf_err;
	      }
	   }
	}
    }
}

double power_of(double d, int i)
{
  int j;
  double tmp = 1;
  if (i > 0)
    {
     for (j = 0; j < i; j++)  tmp *= d;
     return tmp;
    }
  else if(i < 0)
    {
     for (j = 0; j < -i; j++)  tmp *= d;
     return 1./tmp;
    }
  else return 1.;
}

void pre_multipole(struct element* el) /* pre-process multipoles */
{
  /* 
    1. first count multipole components < decapole (cnt)
       if   cnt == 1:
          if    dipole, set el->nc_pos
          else  make n_pole, insert in front, zero el->component
    2. add all comp. > dipole + errors
       if all zero:
          straight quad, yank element
          else  set error count to zero, keep n-pole
       else
          put sum into errors, zero all el->components
  */
  int i, last_nzero = -1, nz_cnt = 0, cnt = 0, n_pole, s_pole = 0, ndmax;
  int low, new_el_t;
  struct element *new_el = NULL;
  char t_list[5][12] = {"dipole", "quadrupole", "sextupole", "octupole",
                        "decapole"};

  ndmax = el->n_values > 22 ? 22 : el->n_values;
  for (i = 12; i < ndmax; i++) 
    {
     if (el->value[i] != zero) 
       {
	s_pole = i; cnt++;
       }
    }
  if (cnt == 1)
    {
     if ((new_el_t = (s_pole-12)/2) == 0)  el->nc_pos = s_pole;
     else
       {
	get_next_name(tmp_name, t_list[new_el_t][0]);
        new_el = new_element(s_pole+1, tmp_name, t_list[new_el_t]);
        for (i = 0; i <= s_pole; i++) new_el->value[i] = el->value[i];
        for (i = 12; i <= s_pole; i++) el->value[i] = 0;
        new_el->flag = s_pole > 13 ? 2 : 1; new_el->npole_sign = 1;
        new_el->keep_in = el->keep_in; 
        new_el->position = el->position;
        new_el->twtab_row = el->twtab_row;
        new_el->na_err = el->na_err; el->na_err = 0;
        new_el->p_al_err = el->p_al_err; el->p_al_err = NULL;
        link_in_front(new_el, el);
        add_to_ellist(new_el);
        strcpy(tmp_name, el->name); strcpy(el->name, new_el->name);
        strcpy(new_el->name, tmp_name);
       }
    }
  for (i = 0; i < FIELD_MAX; i++) tmp_buff[i] = zero;
  low = cnt == 1 ? 2 : 0;
  for (i = low; i < el->n_values-14; i++) 
       tmp_buff[i] = el->value[12+i];
  for (i = 0; i < el->nf_err; i++) tmp_buff[i] += el->p_fd_err->a_dble[i];
  for (i = 0; i < FIELD_MAX; i++) if (tmp_buff[i] != zero) 
    {
     last_nzero = i; nz_cnt++;
    }
  n_pole = last_nzero / 2;
  el->rad_length = el->value[11];
  if (last_nzero < 0)  /* all quad+ components and all errors = zero */
    {
     el->nf_err = 0; 
     if (el->keep_in == 0 && (s_pole == 0 || s_pole > 13)) yank(el);
     else                            el->nc_pos = s_pole;
    }
  else  /* element becomes multipole, all comp. above dipole -> errors */
    {
     el->nc_pos = s_pole > 13 ? 0 : s_pole;
     if (el->ref_delta == zero) 
       {
        el->ref_delta = c1p3;
        el->ref_radius = ref_def;
        el->mult_order = 1;
       }
     if (++last_nzero > el->nf_err)
       {
	if (el->p_fd_err != NULL) strcpy(tmp_name, el->p_fd_err->key);
        else  sprintf(tmp_name,"%s_arfa", el->name);
        el->nf_err = last_nzero;
        el->p_fd_err = make_obj(tmp_name, 0, el->nf_err, 0, 0);
       }
     for (i = 0; i < el->nf_err; i++) el->p_fd_err->a_dble[i] = tmp_buff[i];
     for (i = 14; i < el->n_values; i++)  el->value[i] = zero;
    }
}

void process ()  /* steering routine */
{

  puts("  ++++++++++++++++++++++++++++");
  puts("  +   c6t version 1.0        +");
  puts("  ++++++++++++++++++++++++++++\n");

  read_sequ();   /* from db read sequence, store all elements */
  read_twiss();  /* from db read TWISS table */
  link_twiss();  /* link TWISS table to sequence */
  read_aux();    /* from db read tune, chroma, .. */
  add_drifts();
  conv_elem();   /* tag elements */
  split();       /* convert to thin */
  multi_loop();
  supp_elem();   /* suppress/replace zero force, most markers,
                    and possibly some cavities */
  concat_drifts();
  get_multi_refs();  /* get multipole flag */ 
  equiv_elem();  /* find first equivalent for all elements */ 
  post_multipoles();  /* give errors to all equiv. multipoles */
  block_it();    /* group linear elements into blocks */
  assign_att();  /* assign attributes + errors to all single elements */
  mod_errors();  /* flip normal components */
  write_all_el();
  write_blocks();
  write_struct();
  write_f16_errors();
  write_f34_special();
  write_f3aux();
  write_f8_errors();
}

void pro_elem (int elnum, double pos) 
              /* processes one element, makes linked list */
{
  int j, bits, base_key_list[] = {1, ELPAR_CODE};
  int align_key_list[] = {2, ALIGN_CODE, 0};
  int field_key_list[] = {2, FIELD_CODE, 0};
  char *name;
  char key[KEY_LENGTH], t_key[KEY_LENGTH];
  double corr[2];

#ifdef _call_tree_
       puts("+++++++ pro_elem");
#endif
  name = pseq->names[elnum];
  if ((pelem = doom_fetch(name)) == NULL)
     printf("+=+=+= c6t warning - skip: element %s not in d.b.\n", name);
  else
    {
     lower(pelem->base_name);
     lower(pelem->par_name);
     make_key(pelem->base_name, base_key_list, key);
     if ((pbase = get_obj(key)) == NULL)
        printf("+=+=+= c6t warning - skip: element %s base %s not in d.b.\n", 
               name, pelem->base_name);
     else
       {
	corr[0] = corr[1] = 0;
	doom_gcorrect(name, &pseq->a_int[elnum], &bits, corr);/* corrector? */ 
        if (bits >= 0)
	  {
           for (j = 0; j < 2; j++) pelem->a_dble[j+12] += corr[j];
	  }
        if (strcmp(pelem->base_name, "rbend") == 0) mod_rbend(pelem);
        else if (strcmp(pelem->base_name, "lcavity") == 0) 
            mod_lcavity(pelem);
        else if (strcmp(pelem->base_name, "multipole") == 0) 
            mod_multipole(pelem);
        else if (strcmp(pelem->base_name, "octupole") == 0) 
            mod_octupole(pelem);
        else if (strcmp(pelem->base_name, "quadrupole") == 0) 
            mod_quadrupole(pelem);
        else if (strcmp(pelem->base_name, "sextupole") == 0) 
            mod_sextupole(pelem);
        else if (strcmp(pelem->base_name, "rfcavity") == 0) 
            mod_rfcavity(pelem);
	make_element(pelem);
        strcpy(current_element->org_name, current_element->name);
        current_element->occ_cnt = pseq->a_int[elnum];
        if (pseq->a_int[elnum] > 1)  /* add occurence count to name */
	  {
           sprintf(t_key, "%s+%d", current_element->name,pseq->a_int[elnum]);
           strcpy(current_element->name, t_key);
	  }
        current_element->position = pos;
        add_to_ellist(current_element);
        field_key_list[2] = pseq->a_int[elnum];
        make_key(name, field_key_list, t_key);
        make_dollar_key("UNNAMED_USE", t_key, key);
        if ((current_element->p_fd_err = doom_fetch(key)) != NULL)
          {
           field_cnt++;
           current_element->nf_err = current_element->p_fd_err->c_dble;
           get_error_refs(current_element);
	  }
        align_key_list[2] = pseq->a_int[elnum];
        make_key(name, align_key_list, t_key);
        make_dollar_key("UNNAMED_USE", t_key, key);
        if ((current_element->p_al_err = doom_fetch(key)) != NULL)
          {
           align_cnt++;
           current_element->na_err = current_element->p_al_err->c_dble;
	  }
       }
    }
}

void read_aux()
{
  struct object* p;
  char aux_keys[4][4] = {"QX", "QY", "QX'", "QY'"};
  int i;
  for (i = 0; i < 4; i++)
    {
     if ((p = doom_fetch(aux_keys[i])) == NULL)
       printf("+=+=+= c6t warning - object %s not in d.b.\n", aux_keys[i]);
     else aux_val[i] = p->a_dble[0];
    }
}

void read_sequ()
{
  int j, key_list[] = {1, SEQU_CODE};
  make_key(sequ_name, key_list, key);
  if ((pseq = doom_fetch(key)) == NULL)
    {
     printf("+=+=+= c6t fatal - object %s not in d.b.\n", sequ_name);
     exit(1);
    }
  if ((sequ_no_elem = pseq->c_int) > 0)  sequ_start = pseq->a_dble[0];
  for (j = 0; j < sequ_no_elem; j++)
    {
     if (strchr(pseq->names[j], '$') == NULL)  pro_elem(j, pseq->a_dble[j]);
     else   sequ_end = pseq->a_dble[j];
    }
  sequ_length = sequ_end - sequ_start;
  last_in_sequ = current_element;
}

void read_twiss()
{
  int i;
  int key_list1[] = {1, TABLE_HEAD},key_list2[] = {2, TABLE_BODY,0};
  make_key("TWISS", key_list1, key);
  if ((p_twhead = doom_fetch(key)) == NULL)
    {
     printf("+=+=+= c6t fatal - TWISS header not in d.b.\n");
     exit(1);
    }
  make_key("TWISS", key_list2, key);
  if ((p_twiss = doom_fetch(key)) == NULL)
    {
     printf("+=+=+= c6t fatal - TWISS table not in d.b.\n");
     exit(1);
    }
  s_pos = p_twhead->a_int[s_list_pos("S", p_twhead->names, p_twhead->c_obj)]; 
  for (i = 0; i < N34; i++)
  pos34[i] = 
  p_twhead->a_int[s_list_pos(out_vals[i], p_twhead->names, p_twhead->c_obj)];
}

void remove_from_ellist( /* removes element from correct object list */
		 struct element* p_elem)
{
  int i, j;

#ifdef _call_tree_
  puts("+++++++ remove_from_ellist");
#endif
  for (i = 0; i < types.curr; i++)
   {
    if (strcmp(types.member[i]->base_name, p_elem->base_name) == 0)
       {
        for (j = 0; j < types.member[i]->curr; j++) /* loop over el. in type */
          {
	   if (types.member[i]->elem[j] == p_elem)
	     {
	      types.member[i]->elem[j] 
		= types.member[i]->elem[--types.member[i]->curr];
	      return;           
	     }
          }
       }
    }
}

void replace(struct element* old, struct element* new)
{
  if (old->previous != NULL)  old->previous->next = new;
  new->previous = old->previous;
  if (old->next != NULL)      old->next->previous = new;
  new->next = old->next;
  old->flag = 0;
}

int s_list_pos(char* p, char* vlist[], int length)
{
  int i;
  for (i = 0; i < length; i++) if (strcmp(p, vlist[i]) == 0) return i;
  return -1;
}

void split()
{
  int i;
  struct element* el;
  if (split_list != NULL)
    {
     for (i = 0; i < split_list->curr; i++)
       {
	el = split_list->elem[i];
        if (el->flag == 1 
            && (split_flag != 0 || el->nf_err > 0)) split_special(el);
        else if (el->flag == 2 || el->flag == 3)  split_other(el);
        else if (el->split == 3)  split_kicker(el);
       }
    }
}

void split_kicker(struct element* el)
{
  struct element *k1, *k2;
  char c[24];
  int af;
  if (el->value[0] > zero) split_other(el);
  if (el->flag == 6) /*split kicker into h + v */
    {
     af = get_next_name(c, 'h');
     k1 = new_element(13, c, "hkicker");
     af = get_next_name(c, 'v');
     k2 = new_element(14, c, "vkicker");
     k1->force = k2->force = el->force;
     k1->value[12] = el->value[12];
     k2->value[13] = el->value[13];
     k1->flag = k2->flag = 5;
     k1->position = el->position;
     k2->position = el->position;
     replace(el, k1);
     link_behind(k2, k1);
     add_to_ellist(k1); add_to_ellist(k2);
    }
}

void split_other(struct element* el)
{
/* -> two drifts with non-lin. thin lens at centre */
  struct element *d1, *d2;
  double length = el->value[0] / two; 
  char c[24];
  int af;

  af = get_next_name(c, 'd');
  d1 = new_element(1, c, "drift");
  af = get_next_name(c, 'd');
  d2 = new_element(1, c, "drift");
  d1->value[0] = d2->value[0] = length;
  d1->flag = d2->flag = 1;
  d1->position = el->position - d1->value[0] / two;
  d2->position = el->position + d2->value[0] / two;
  treat_split(el);
  link_in_front(d1, el);
  link_behind(d2, el);
  add_to_ellist(d1); add_to_ellist(d2);
}

void split_special(struct element* el) 
/* -> two lin. halves with multipole at centre */
{
  struct element *d1, *mt;
  double length = el->value[0] / two, mt_position = el->position; 
  char c[24];
  int j, af;

  if (el->nf_err > 0) 
    app_factor(el->value[0], el->p_fd_err->a_dble, el->nf_err);
  af = get_next_name(c, *el->base_name);
  d1 = new_element(el->n_values, c, el->base_name);
  d1->value[0] = el->value[0] = length;
  for (j = 1; j < el->n_values; j++) 
      d1->value[j] = el->value[j];
  d1->flag = el->flag = 1;
  d1->force = el->force = 1;
  d1->position = el->position + d1->value[0] / two;
  el->position = el->position - el->value[0] / two;
  af = get_next_name(c, 'm');
  mt = new_element(MULTI_MAX, c, "multipole");
  mt->force = 1;
  mt->flag = 2; mt->position = mt_position;
  mt->n_values = el->n_values; /* multipole forces remain zero */
  mt->p_fd_err = el->p_fd_err; el->p_fd_err = NULL;
  mt->nf_err = el->nf_err; el->nf_err = 0;
  mt->p_al_err = el->p_al_err; mt->na_err = el->na_err;
  el->p_al_err = NULL; el->na_err = 0;
  mt->keep_in = split_flag;
  add_to_ellist(mt);
  add_to_ellist(d1);
  link_behind(mt, el);
  link_behind(d1, mt);
}

void supp_elem()
{
  struct element *d1, *el;
  char c[24];
  int af;

  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     el = current_element;
     if (el->value[0] == zero)  /* zero length */
       {
	 if (el->flag == 0)  yank(el); 
         else if (el->keep_in == 0 && el->npole_sign == 0 
                  && (el->flag == 1 || el->flag > 4)
                  && ident_zero(el) == 0) yank(el);
         else if (el->flag == 3) /* cavity */
	   {
	    cavity_count++;
	    if (cavall_flag == 0 && cavity_count > 1) yank(el);
	   }
       }
     else if(el->c_drift > 0)
       {
        af = get_next_name(c, 'd');
        d1 = new_element(1, c, "drift");
        d1->value[0] = el->value[0];
        d1->flag = 1; d1->position = el->position;
        replace(el, d1);
        if (af != 0)  add_to_ellist(d1);
       }
     current_element = current_element->next;
    }
}

void supp_small_comp(struct object* p)
{
  int i;
  for (i = 12; i < p->c_dble-1; i+=2)
    {
     if (fabs(p->a_dble[i]) > fabs(p->a_dble[i+1]))
       {
         if (fabs(p->a_dble[i+1]) / fabs(p->a_dble[i]) < eps_6)
           p->a_dble[i+1] = zero;
       }
     else if (fabs(p->a_dble[i+1]) > fabs(p->a_dble[i]))
       {
         if (fabs(p->a_dble[i]) / fabs(p->a_dble[i+1]) < eps_6)
           p->a_dble[i] = zero;
       }
    }
}

void treat_split(struct element* el)
{
  if (el->flag == 2)  el->keep_in = 1;
  if (el->nf_err > 0)
    {
     make_multipole(el);
    }
  else 
    {
     if (el->force != 0)
       app_factor(el->value[0], &el->value[12], el->n_values-12);
     el->value[0] = zero; /* set element length to zero */
    }
}

void yank(struct element* el)
{
  if (el->previous != NULL)  el->previous->next = el->next;
  else                       first_in_sequ      = el->next;
  if (el->next != NULL)      el->next->previous = el->previous;
  else                       last_in_sequ       = el->previous;
  el->flag = 0;
}

void write_all_el()
{
  char title[] =
  "SINGLE ELEMENTS---------------------------------------------------------";
  f2 = fopen("fc.2", "w");
  fprintf(f2, "%s\n", title);
  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     if (current_element->flag > 0 
         && current_element == current_element->equiv
         && current_element->w_flag == 0)
       write_element(current_element);
     current_element = current_element->next;
    }
  fprintf(f2, "NEXT\n");
}

void write_element(struct element* el)
{
  if (strcmp(el->name, "CAV") != 0)
      fprintf(f2, "%-16s %2d  %16.9e %17.9e  %17.9e\n", 
          el->name, el->out_1, el->out_2, el->out_3, el->out_4);
  el->w_flag = 1;
}

void write_blocks()
{
  char title1[] =
  "BLOCK DEFINITIONS-------------------------------------------------------";
  char title2[] = "1  1";
  struct block* p = first_block;
  int i, lc = 0, nbct = 0;
  double sum = 0;

  fprintf(f2, "%s\n", title1); fprintf(f2, "%s\n", title2);
  while (p != NULL)  
    {
     if (p->equiv == p)
       {
        if (p->flag != 0)
          {
           sprintf(p->name, "BLOC%d", ++nbct);
           fprintf(f2, "%-18s", p->name); lc++;
           for (i = 0; i < p->elements->curr; i++)
	     {
              if (lc++ == LINES_MAX)
	       {
                fprintf(f2,"\n"); fprintf(f2,"                  "); lc = 2;
	       }
	      fprintf(f2, "%-18s",p->elements->elem[i]->equiv->name);
	     }
          }
        if (lc > 0)
          {
           fprintf(f2,"\n"); lc = 0;
          }
       }
     sum += p->length;
     p = p->next;
    }
  printf("\ntotal block length: %f\n", sum);
  fprintf(f2, "NEXT\n");
}

void write_f8_errors()
{
  if (align_cnt == 0)  return;
  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     if (current_element->na_err > 0)
       {
        if (f8_cnt++ == 0)    f8 = fopen("fc.8", "w");
        fprintf(f8, "%-16s  %14.6e%14.6e%17.9e\n",current_element->equiv->name,
	       1000*current_element->p_al_err->a_dble[0],
	       1000*current_element->p_al_err->a_dble[1],
	       1000*current_element->p_al_err->a_dble[2]);
       }
     current_element = current_element->next;
    }
}

void write_f16_errors()
{
  int i;
  double factor;

  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     if (current_element->nf_err > 0 && current_element->ref_delta != zero)
       {
        if (f16_cnt++ == 0)    f16 = fopen("fc.16", "w");
        if (current_element->equiv->f3_flag++ == 0)
	  write_f3_entry("multipole", current_element->equiv);
	fprintf(f16,"%s\n", current_element->equiv->name);
        for (i = 0; i < current_element->nf_err; i++) 
            tmp_buff[i] = current_element->p_fd_err->a_dble[i];
        for (i = current_element->nf_err; i < FIELD_MAX; i++) 
            tmp_buff[i] = zero;
        factor = c1p3 / current_element->ref_delta;
        for (i = 0; i < FIELD_MAX/2; i++)
	  {
	   fprintf(f16, "%23.15e", factor*tmp_buff[2*i]);
           factor *= current_element->ref_radius / (i+1);
           if ((i+1)%3 == 0) fprintf(f16,"\n");
	  }
        if (i%3 != 0) fprintf(f16,"\n");
        factor = c1p3 / current_element->ref_delta;
        for (i = 0; i < FIELD_MAX/2; i++)
	  {
	   fprintf(f16, "%23.15e", factor*tmp_buff[2*i+1]);
           factor *= current_element->ref_radius / (i+1);
           if ((i+1)%3 == 0) fprintf(f16,"\n");
	  }
        if (i%3 != 0) fprintf(f16,"\n");
       }
     current_element = current_element->next;
    }
}

void write_f34_special()
{
  int i, j, n, add, flags[FIELD_MAX];
  double values[FIELD_MAX];
  char* t_list[NT34];
  char t_name[24];
  char* cp;
  
  t_list[0] = &mpole_names[1][0];
  t_list[1] = &mpole_names[2][0];
  t_list[2] = &mpole_names[3][0];
  t_list[3] = &mpole_names[4][0];
  t_list[4] = &mpole_names[5][0];
  
  if (special_flag == 0 || p_twhead == NULL || p_twiss == NULL)  return;

  /* Twiss header object:
     # rows: twiss_rows above
     # columns: p_twhead->c_obj
     # column names: p_twhead->names
     # column name positions: p_twhead->a_int

     Twiss table object:
     # element names: p_twiss->names
     # occurrence counts: p_twiss->a_int
  */

  current_element = first_in_sequ;
  while (current_element != NULL)
    {
     for (i = 0; i < NT34; i++)
       {
        if (strcmp(current_element->base_name, t_list[i]) == 0)
	  {
	   n = f34_values(current_element, flags, values);
           add = current_element->twtab_row * p_twhead->c_obj;  
           if (f34_cnt++ == 0)    f34 = fopen("fc.34", "w");
           for (j = 0; j < n; j++)
	     {
	      strcpy(t_name, current_element->name);
	      if ((cp = strchr(t_name, '+')) != NULL) *cp = '\0';
              fprintf(f34, 
              " %20.13e  %-16s %3d %20.13e %20.13e %20.13e %20.13e %20.13e\n",
              p_twiss->a_dble[add+s_pos], 
	      t_name, 
	      flags[j],
	      values[j],
	      p_twiss->a_dble[add+pos34[0]],
	      p_twiss->a_dble[add+pos34[1]],
	      p_twiss->a_dble[add+pos34[2]],
              p_twiss->a_dble[add+pos34[3]]);
	     }
	  }
       }
     current_element = current_element->next;
    }
  add = (twiss_rows-1) * p_twhead->c_obj;
  fprintf(f34, 
  " %20.13e  %-16s %3d %20.13e %20.13e %20.13e %20.13e %20.13e\n",
  p_twiss->a_dble[add+s_pos], 
  "end_marker", 
  100,
  zero,
  p_twiss->a_dble[add+pos34[0]],
  p_twiss->a_dble[add+pos34[1]],
  p_twiss->a_dble[add+pos34[2]],
  p_twiss->a_dble[add+pos34[3]]);
}

void write_f3_entry(char* option, struct element* el)
{
  if (f3_cnt++ == 0)     f3  = fopen("fc.3", "w");
  if (strcmp(option, "multipole") == 0) write_f3_mult(el);
}

void write_f3aux()
{
  double* b = p_beam->a_dble;
  if (p_beam != NULL)
    {
     if (f3aux_cnt++ == 0)     f3aux  = fopen("fc.3.aux", "w");
     fprintf(f3aux, "SYNC\n");
     fprintf(f3aux,"%12.0f%10.6f%10.3f 0.%10.3f%12.6f  1\n",
	     harmon, b[36], total_voltage,
             sequ_length, c1p3*b[0]);
     fprintf(f3aux,"      1.        1.\n");
     fprintf(f3aux, "NEXT\n");
     fprintf(f3aux, "BEAM\n");
     fprintf(f3aux, "%12.4e%14.6g%14.6g%12.4e%12.4e  1  0\n",
             b[1]*b[13], 0.25e6*b[6], 0.25e6*b[8], b[10], b[11]);
     fprintf(f3aux, "NEXT\n");
    }
  if (aux_val[0] > -1.e10 && aux_val[1] > -1.e10)
    {
     fprintf(f3aux, "TUNE\n");
     fprintf(f3aux, "QF%12.5f\n", aux_val[0]);
     fprintf(f3aux, "QD%12.5f\n", aux_val[1]);
     fprintf(f3aux, "NEXT\n");
    }
  if (aux_val[2] > -1.e10 && aux_val[3] > -1.e10)
    {
     fprintf(f3aux, "CHRO\n");
     fprintf(f3aux, "SXF%12.5f\n", aux_val[2]);
     fprintf(f3aux, "SXD%12.5f\n", aux_val[3]);
     fprintf(f3aux, "NEXT\n");
    }
}

void write_f3_mult(struct element* el)
{
  int i, j, i_max = -1;
  struct element* eln;
  if (multi_type < 0)  return;
  fprintf(f3,"MULT\n");
  fprintf(f3,"%-16s%20.10e%20.10e\n", el->name, c1p3*el->ref_radius,
          el->ref_delta);
  /* find non-zero errors in all elements equiv. to this, print error matrix */
  for (i = 0; i < FIELD_MAX; i++) error_matrix[i] = zero;
  for (j = 0; j < types.member[multi_type]->curr; j++)
     {
      eln = types.member[multi_type]->elem[j];
      if (eln->equiv == el)
  	{
	  for (i = 0; i < eln->nf_err; i++)
	    {
             if (eln->p_fd_err->a_dble[i] != zero)
	       {
                i_max = i; error_matrix[i] = 1.;
	       }
	    }
  	}
     }
  if (++i_max > 0)  i_max += i_max%2;
  for (i = 0; i < i_max; i++) 
    {
     fprintf(f3,"%4.0f.%4.0f.", 0., error_matrix[i]);
     if ((i+1)%2 == 0) fprintf(f3,"\n");
    }
  fprintf(f3,"NEXT\n");
}

void write_struct()
{
  struct block* p = first_block;
  int lc = 0;
  char* out;
  char title[] =
  "STRUCTURE INPUT---------------------------------------------------------";

  fprintf(f2, "%s\n", title);
  while (p != NULL)  
    {
     if (p->flag == 0) out = p->first->equiv->name;
     else              out = p->equiv->name;
     if (lc++ == LINES_MAX)
      {
       fprintf(f2,"\n"); lc = 1;
      }
     fprintf(f2, "%-18s", out);
     p = p->next;
    }
  if (lc > 0)
    {
     fprintf(f2,"\n");
    }
  fprintf(f2, "NEXT\n");
}
