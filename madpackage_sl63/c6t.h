#define BASE_TYPES 100    /* maximum no. of element types allowed */
#define EL_COUNT   100    /* initial length of element type list */
struct element
{
  char name[48],           /* name including occurrence count if > 1 */
  org_name[48],            /* original name */
  base_name[48];           /* basic type */
  struct element* previous;
  struct element* next;
  struct element* equiv;   /* pointer to first identical element */
  int flag;                /* treatment flag_1 or _2 or _3 (see type_info) */
  int force;               /* magnet flag (flag_5) (see type_info) */
  int c_drift;             /* treatment flag_4 (see type_info) */
  int split;               /* treatment flag_6 (see type_info) */
  int n_values;            /* length of value */
  int w_flag;              /* 0 if not, 1 if written on fort.2 */
  int out_1;               /* output parameter 1, fort.2 */
  int na_err;              /* current no. of alignment errors */
  int nf_err;              /* current no. of field errors */
  int nc_pos;              /* component count, only multipoles */
  int npole_sign;          /* sign inversion flag for even (created) npoles */
  int keep_in;             /* if not 0, do not yank */
  int mult_order;          /* error reference comp., only multipoles */
  int f3_flag;             /* for multipole def. on fc.3 */
  int occ_cnt;             /* occurrence count */
  int twtab_row;           /* row number in twiss table */
  double position;         /* s position in sequence [m] */
  double rad_length;       /* radiation length of multipoles [m] */
  double ref_radius;       /* reference radius for multipole errors [m] */
  double ref_delta;        /* reference delta for multipole errors */
  double out_2;            /* output parameter 2, fort.2 */
  double out_3;            /* output parameter 3, fort.2 */
  double out_4;            /* output parameter 4, fort.2 */
  double* value;           /* element strength etc. values */
  struct object* p_al_err; /* pointer to alignment error object */
  struct object* p_fd_err; /* pointer to field error object */
};

struct el_list /* contains list of element pointers */
{
  int max,                /* max. pointer array size */
      curr;               /* current occupation */
  char base_name[48];
  struct element** elem; /* element pointer list */
};

struct ref_list /* contains mult. names, orders, ref. radii */
{
  int size;                /*  array size */
  char** names;
  int* index;              /* sort index */
  int* orders;
  double* radii;
};

struct block
{
  char name[48];
  double length;
  int flag;              /* if 0 take element, else block */
  struct element* first;
  struct element* last;
  struct block* previous;
  struct block* next;
  struct block* equiv;
  struct el_list* elements;
};

struct li_list /* contains list of list pointers */
{
  int curr;               /* current occupation */
  struct el_list* member[BASE_TYPES]; /* list pointer list */
};

struct type_info /* info about types */
{
  char name[48];    /* base_type */
  /* flag meanings - 0: skip, 1: linear, >1: non-linear, 
                     2: convert to multipole (temporarily), 3: cavity 
                     4: make 2 if in explicit list, else skip
                     5: only split */
  int      flag_1,  /* for length = 0 */
           flag_2,  /* for length > 0, normal */
           flag_3,  /* for length > 0, skew */
           flag_4,  /* if > 0: make drift, print warning when encountered */
           flag_5,  /* if > 0: magnet (for k0n * l) */
           flag_6;  /* if length > 0: 0 = no split
                       1 = split; if flag_2(_3) = 1: two identical + zero m.
                                  if flag_2(_3) = 2: two drift + full m. */
};

