      program get_corr
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      integer k, iargc, iostat
      character * 12 filename, temp
      character * 120 doom_name
      data filename / 'current.doom' /
      k = iargc()
      if (k .gt. 0)  then
        call getarg(1, temp)
        prnt = temp(:2) .eq. '-p'
      else
        prnt = .false.
      endif
      print *, '+++ get_corr: start of execution +++'
      open(11,file=filename, iostat=iostat)
      if (iostat .ne. 0)  then
        print *,'+++ fatal: cannot open file: ',filename
      else
        read(11, '(a)') doom_name
        close(11)
        call init()
        call doom_open(doom_name)
        open(12,file='corr.settings',status='UNKNOWN')
        call gq_names()
        call gc_names()
        call twiss()
        call correct_a()
        call correct_b()
        call doom_close()
        write(12,'(a)') 'return'
        close(12)
        print *, '+++ get_corr finished +++'
      endif
      end
      subroutine init()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      integer i, j, ipc(8)
      character * 1 clip(8)
      integer nc(m_pips), ic(m_comp, m_pips)
      data nc / 2, 2, 1/
      data ic /
     +          -3, -4,
     +           3, 6,
     +           4, 0 /
      data ipc / 4, 0, 0, 0, 4, 0, 0, 0/
      data clip / '1', '2', '3', '4', '5', '6', '7', '8' /
      do i = 1, 8
        split(i) = ipc(i)
        cip(i) = clip(i)
      enddo
      cside(1) = 'L'
      cside(2) = 'R'
      do i = 1, m_pips
        nc_corr(i) = nc(i)
        do j = 1, m_comp
          ic_corr(j,i) = ic(j,i)
        enddo
      enddo
      end
      subroutine gc_names()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      character * 16 tmp, tmp2, corr(m_pips)
      character * 4  fmt1, fmt2
      integer i, j, k, k1, k2, n, lastnb
      data fmt1, fmt2 / '(i1)', '(i2)' /
      data corr/ 'MC.Q2.xx', 'MC.Q3.xx', 'MC.Q4.xx' /
 
      do i = 1,m_pips
        do j = 1,2
          do k = 1,8
            c_names(i,j,k) = ' '
          enddo
        enddo
      enddo
      do i = 1, 8
        n = split(i)
        if (n .gt. 0)  then
          do k = 1, 2
            do k1 = 1, m_pips
              j = lastnb(corr(k1))
              corr(k1)(j-1:j-1) = cside(k)
              corr(k1)(j:j) = cip(i)
              c_names(k1,k,i) = corr(k1)
              do k2 = 1, nc_corr(k1)
                if (ic_corr(k2,k1) .gt. 0)  then
                  tmp = 'B'
                else
                  tmp = 'A'
                endif
                if (abs(ic_corr(k2,k1)) .lt. 10)  then
                  write(tmp(2:2), fmt1) abs(ic_corr(k2,k1))
                  j = 3
                else
                  write(tmp(2:3), fmt2) abs(ic_corr(k2,k1))
                  j = 4
                endif
                tmp(j:j) = '.'
                tmp2 = tmp(:j) // corr(k1)
                bc_names(k2,k1,k,i) = tmp2
              enddo
            enddo
          enddo
        endif
      enddo
      end
      subroutine gq_names()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      character * 16 tmp1, tmp2
      integer i, j, k, l, n
      data tmp1, tmp2 / 'MQXA.xxx..x', 'MQXB.x2xx..x' /
 
      n_names = 0
      do i = 1, 8
        n = split(i)
        if (n .gt. 0)  then
          write(tmp1(8:8), '(i1)') i
          write(tmp2(9:9), '(i1)') i
          do j = 1, n
            write(tmp1(11:11), '(i1)') j
            write(tmp2(12:12), '(i1)') j
            do k = 1, 2
              if (k .eq. 1)  then
                tmp1(7:7) = 'L'
                tmp2(8:8) = 'L'
              else
                tmp1(7:7) = 'R'
                tmp2(8:8) = 'R'
              endif
              do l = 1, 2
                if (l .eq. 1)  then
                  tmp1(6:6) = '1'
                  tmp2(6:6) = 'A'
                else
                  tmp1(6:6) = '3'
                  tmp2(6:6) = 'B'
                endif
                q_names(n_names+1) = tmp1
                q_names(n_names+2) = tmp2
                n_names = n_names + 2
              enddo
            enddo
          enddo
        endif
      enddo
      end
      subroutine twiss()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      integer nrow
      integer i, j, k, n, pos_flag, doom_gpos, side, last
      integer lastnb
      if(prnt)  then
        print *,'quadrupoles'
        print '(5a16)',(q_names(i), i = 1, n_names)
        print *,'correctors'
        do k = 1, 8
          if (split(k) .ne. 0)
     +    print '(4a16)', ((c_names(i,j,k),i=1,m_pips),j=1,2)
        enddo
      endif
*-- set suml to the maximum summ length you will accept
      suml = 17 * 25
*-- set k to the length of one name as declared above
      k = mn
*-- store the table name in a variable - this is the safest procedure,
*   provided t_name contains trailing blanks
      t_name = 'TWISS'
*-- get the table header
      call doom_gthead(t_name, k, sequ_name, ndelta, nrow,
     +pos_flag, suml, summ)
      if (nrow .lt. 0)  then
        print *, 'TWISS header not found'
      else
        ncolumn = mc
        nrow = mr
*-- call with the deltap value for which the table is requested
        call doom_gtbody('TWISS ', summ(1,1), sequ_name, ncolumn,
     +  nrow, k, names, occ, table)
        if (nrow .lt. 0)  then
          print *, 'TWISS table not found for deltap: ', summ(1,1)
        else if (nrow .eq. mr)  then
          print *, 'TWISS table too big, exit'
        else
          if (prnt) print *, 'Twiss table length: ',nrow
*-- use the doom_gpos function to find the columns for s, betx, bety
          s_pos = doom_gpos('S ')
          betx_pos = doom_gpos('BETX ')
          bety_pos = doom_gpos('BETY ')
          nextr = 0
          do i = 1, nrow
            do k = 1, n_names
              if (names(i) .eq. q_names(k))  then
                nextr = nextr + 1
                q_ref(nextr) = i
                last = lastnb(q_names(k))
                err_fl(nextr) = 100
                call doom_gfield(names(i), occ(i), err_fl(nextr),
     +          errors(1,1,nextr))
              endif
              do j = 1, 8
                if (split(j) .gt. 0)  then
                  do side = 1, 2
                    do n = 1, m_pips
                      if (names(i) .eq. c_names(n,side,j)) then
                        c_ref(n,side,j) = i
                      endif
                    enddo
                  enddo
                endif
              enddo
            enddo
          enddo
        endif
      endif
      end
      subroutine correct_a()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      integer i, j, k, kk, last, side
      integer lastnb, icomp, ncorr, jcorr
      double precision cbetx(2), cbety(2), pow1, pow2, csumx, csumy
      double precision bx, by, err, c1, c2, phase, a11, a12, a21, a22,
     +den,zero,c1s,c2s,tmp,sumerr
      parameter (zero = 0.d0)
      do j = 1, 8
        if (split(j) .gt. 0)  then
          do ncorr = 1, m_pips
            sumerr = zero
            do 10  jcorr = 1, nc_corr(ncorr)
              icomp = ic_corr(jcorr,ncorr)
              if (icomp .ge. 0)  goto 10
              icomp = -icomp
              kk = 2
              pow1 = (icomp-1)/2.d0
              pow2 = (icomp-2)/2.d0
              phase = (-1)**icomp
              if (prnt) print *, 'icomp, phase: ', icomp, phase
              do side = 1, 2
                cbetx(side) = table(betx_pos,c_ref(ncorr,side,j))
                cbety(side) = table(bety_pos,c_ref(ncorr,side,j))
                tmp = cbetx(side)
                if (phase .gt. zero)  then            ! n even
                  cbetx(side) = phase**side
     &            * cbetx(side)**pow2 * sqrt(cbety(side))
                  cbety(side) = phase**side
     &            * sqrt(tmp) * cbety(side)**pow2
                else                                  ! n odd
                  cbetx(side) = phase**side
     &            * cbetx(side)**pow2 * sqrt(cbety(side))
                  cbety(side) = phase**side * cbety(side)**pow1
                endif
              enddo
              if (prnt)  then
                print *, cbetx
                print *, cbety
              endif
              den = cbetx(1)*cbety(2) - cbety(1)*cbetx(2)
              a11 = cbety(2) / den
              a12 = -cbetx(2) / den
              a21 = -cbety(1) / den
              a22 = cbetx(1) / den
              csumx = 0
              csumy = 0
              do side = 1, 2
                do i = 1, nextr
                  do k = 1, n_names
                    if (names(q_ref(i)) .eq. q_names(k))  then
                      last = lastnb(q_names(k))
                      if (q_names(k)(last-3:last-3) .eq. cip(j) .and.
     +                q_names(k)(last-4:last-4) .eq. cside(side))  then
                        err = errors(kk,icomp,i)
                        sumerr = sumerr + phase**side * err
                        bx = table(betx_pos,q_ref(i))
                        by = table(bety_pos,q_ref(i))
                        if (prnt .and. err .ne. 0.d0)
     +                  print '(a16, ''IP'',i1, ''-'', i1, 1p, 3d14.6)',
     +                  names(q_ref(i)), j, side, bx, by, err
                        if (phase .gt. zero)  then            ! n even
                          csumx = csumx + phase**side
     &                    * err * bx**pow2 * sqrt(by)
                          csumy = csumy + phase**side
     &                    * err * sqrt(bx) * by**pow2
                        else                                  ! n odd
                          csumx = csumx + phase**side
     &                    * err * bx**pow2 * sqrt(by)
                          csumy = csumy + phase**side
     &                    * err * by**pow1
                        endif
                      endif
                    endif
                  enddo
                enddo
              enddo
              c1 = -(a11 * csumx + a12 * csumy)
              c2 = -(a21 * csumx + a22 * csumy)
              c2s = -csumx / (2*cbetx(2))
              c1s = -csumx / (2*cbetx(1))
              if (prnt)  print *, 'csumx, csumy, sumerr: ',
     &        csumx, csumy, sumerr
*              write(12, '(a, '':= '', e16.8)')
*     +        bc_names(jcorr,ncorr,1,j)(:13), c1s
*              write(12, '(a, '':= '', e16.8)')
*     +        bc_names(jcorr,ncorr,2,j)(:13), c2s
              write(12, '(a, '':= '', e16.8)')
     +        bc_names(jcorr,ncorr,1,j)(:13), c1
              write(12, '(a, '':= '', e16.8)')
     +        bc_names(jcorr,ncorr,2,j)(:13), c2
   10       continue
          enddo
        endif
      enddo
      end
      subroutine correct_b()
*-----------------------------------------------------------------------
      implicit none
      integer mr, mc, mn, mquad
*-- mc below is the maximum of columns for TWISS
      parameter (mr = 15000, mc = 29, mn = 16,
     +mquad = 200)
      integer m_comp, m_pips
      parameter (m_comp = 2, m_pips = 3)
      common/commi/n_names, ndelta, ncolumn, suml, nextr,
     +s_pos, betx_pos, bety_pos,
     +nc_corr(m_pips), ic_corr(m_comp, m_pips), split(8),
     +err_fl(mquad), occ(mr), q_ref(mquad), c_ref(m_pips,2,8)
      integer occ, ndelta, ncolumn, suml, nextr
      integer err_fl, n_names, q_ref, c_ref, split
      integer s_pos, betx_pos, bety_pos, nc_corr, ic_corr
      common/commr/ summ(17,25), table(mc, mr), errors(2,50,mquad)
      double precision summ, table, errors
      common/commc/t_name, sequ_name, names(mr), q_names(mquad),
     +c_names(m_pips,2,8), bc_names(m_comp,m_pips,2,8), cip(8),
     +cside(2)
      character * (mn) q_names, c_names, bc_names
      character *(mn) t_name, sequ_name, names
      character * 1 cip, cside
      common/comml/prnt
      logical prnt
      integer i, j, k, kk, last, side
      integer lastnb, icomp, ncorr, jcorr
      double precision cbetx(2), cbety(2), pow1, pow2,csumx, csumy
      double precision bx, by, err, den, a11, a12, a21, a22, c1, c2,
     +phase,zero,c1s,c2s,tmp,sumerr
      parameter (zero = 0.d0)
      do j = 1, 8
        if (split(j) .gt. 0)  then
          do ncorr = 1, m_pips
            do 10 jcorr = 1, nc_corr(ncorr)
              sumerr = zero
              icomp = ic_corr(jcorr,ncorr)
              if (icomp .le. 0)  goto 10
              kk = 1
              pow1 = (icomp-1)/2.d0
              pow2 = (icomp-2)/2.d0
              phase = (-1)**icomp
              if (prnt) print *, 'icomp, phase: ', icomp, phase
              do side = 1, 2
                cbetx(side) = table(betx_pos,c_ref(ncorr,side,j))
                cbety(side) = table(bety_pos,c_ref(ncorr,side,j))
                tmp = cbetx(side)
                if (phase .gt. zero)  then            ! n even
                  cbetx(side) = phase**side * cbetx(side)**pow1
                  cbety(side) = phase**side * cbety(side)**pow1
                else                                  ! n odd
                  cbetx(side) = phase**side * cbetx(side)**pow1
                  cbety(side) = phase**side
     &            * sqrt(tmp) * cbety(side)**pow2
                endif
              enddo
              if (prnt)  then
                print *, cbetx
                print *, cbety
              endif
              den = cbetx(1)*cbety(2) - cbety(1)*cbetx(2)
              a11 = cbety(2) / den
              a12 = -cbetx(2) / den
              a21 = -cbety(1) / den
              a22 = cbetx(1) / den
              csumx = 0
              csumy = 0
              do side = 1, 2
                do i = 1, nextr
                  do k = 1, n_names
                    if (names(q_ref(i)) .eq. q_names(k))  then
                      last = lastnb(q_names(k))
                      if (q_names(k)(last-3:last-3) .eq. cip(j) .and.
     +                q_names(k)(last-4:last-4) .eq. cside(side))  then
                        err = errors(kk,icomp,i)
                        sumerr = sumerr + phase**side * err
                        bx = table(betx_pos,q_ref(i))
                        by = table(bety_pos,q_ref(i))
                        if (prnt .and. err .ne. 0.d0)
     +                  print '(a16, ''IP'',i1, ''-'', i1, 1p, 3d14.6)',
     +                  names(q_ref(i)), j, side, bx, by, err
                        if (phase .gt. zero)  then            ! n even
                          csumx = csumx + phase**side * err * bx**pow1
                          csumy = csumy + phase**side * err * by**pow1
                        else                                  ! n odd
                          csumx = csumx + phase**side * err * bx**pow1
                          csumy = csumy + phase**side
     &                    * err * sqrt(bx) * by**pow2
                        endif
                      endif
                    endif
                  enddo
                enddo
              enddo
              c1 = -(a11 * csumx + a12 * csumy)
              c2 = -(a21 * csumx + a22 * csumy)
              c2s = -csumx / (2*cbetx(2))
              c1s = -csumx / (2*cbetx(1))
              if (prnt)  print *, 'csumx, csumy, sumerr: ',
     &        csumx, csumy, sumerr
*              write(12, '(a, '':= '', e16.8)')
*     +        bc_names(jcorr,ncorr,1,j)(:13), c1s
*              write(12, '(a, '':= '', e16.8)')
*     +        bc_names(jcorr,ncorr,2,j)(:13), c2s
              write(12, '(a, '':= '', e16.8)')
     +        bc_names(jcorr,ncorr,1,j)(:13), c1
              write(12, '(a, '':= '', e16.8)')
     +        bc_names(jcorr,ncorr,2,j)(:13), c2
   10       continue
          enddo
        endif
      enddo
      end
      integer function lastnb(string)
*-----------------------------------------------------------------------
*
*--- find last non-blank in string
*--- input
*    STRING
*--- output
*    function value = last non-blank (at least 1)
*-----------------------------------------------------------------------
      implicit none
      integer i
      character *(*) string
      do i = len(string), 1, -1
        if (string(i:i) .ne. ' ')  goto 20
      enddo
      i = 1
   20 lastnb = i
      end
