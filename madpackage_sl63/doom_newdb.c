#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <time.h>
#ifdef __LINUX__
#include "doomex.h"
#include "doom.h"
#endif
#ifndef __LINUX__
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doomex.h"
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doom.h"
#endif

void put_elpar(char*, char*, int*, int, int);
void store_dict();
void store_eltab();
void store_tables();
void type_table();
int trim(char*);

char key[KEY_LENGTH], text[100], glist[10000];
char *colon, *eofp, *cp1, *cp2;
FILE *fp;
int gcode[1000];
double* ddummy;

void main(int argc, char **argv)
{
  if (argc < 3)  
    {
     puts("usage: doom_newdb start_file d.b.name");
     exit(0);
    }
  doom_open(argv[2]);
  fp = fopen(argv[1], "r");
  if (fp == NULL)
    {
     printf("cannot open file %s\n", argv[1]);
     exit(1);
    }
  store_dict();
  store_eltab();
  store_tables();
  doom_close();
}

void type_table() /* reads sorted (for names) (!!) reference table */
                  /* per line: (int)reference  name */
{
  int i, c_char, t_leng, length = TABLE_OFFSET;
  char type[12];
  char* cp = glist;
  char lkey[KEY_LENGTH], t_key[KEY_LENGTH];
  int key_list[] = {1, TABLE_CODE};
  struct object* p;

  /* first line already in text buffer */
  sscanf(text, "%s%s%d", type, t_key, &t_leng);
  for (i = 0; i < TABLE_OFFSET; i++)
    {
     *cp++ = '*'; *cp++ = '\0';  /* fill dummy names at start */
    }
  while ((eofp = fgets(text, 100, fp)) != NULL 
         && strncmp(text, "<<<<>>>>", 8) != 0)
    {
     sscanf(text, "%d%s", &gcode[length++], cp);
     cp += strlen(cp) + 1;
    }
  c_char = cp - glist;
  make_key(t_key, key_list, lkey);
  p = make_obj(lkey, length, 0, c_char, 0);
  gcode[0] = TABLE_OFFSET; gcode[1] = t_leng;
  strcpy(p->obj_type, type);
  fill_obj(p, length, 0, c_char, gcode, ddummy, glist);
  doom_save(p);
}

void put_elpar(char* key, char* list, int code[], int length, int cl)
{
  char lkey[KEY_LENGTH];
  int key_list[] = {1, ELPAR_CODE};
  struct object* p;

  make_key(key, key_list, lkey);
  p = make_obj(lkey, length, 0, cl, length);
  fill_obj(p, length, 0, cl, code, ddummy, list);
  p->c_obj = p->l_obj;
  doom_save(p);
}

void store_dict()
{
  char name[] = "MAD-8-DICT";
  char *eofp;
  int n, skip = 0, count = 0;

  while ((eofp = fgets(text, 100, fp)) != NULL 
         && strncmp(text, "<<<<>>>>", 8) != 0)
    {
     count++;
     if ((n = trim(text)) <= 0)  skip++;
     else  doom_pline(name, text, &n);
    }
  printf("%d dictionary lines read of which %d empty\n", count, skip);
}

void store_eltab()
{
  char *colon, *eofp, *cp1, *cp2;
  int c, n, length, skip = 0, count = 0;

  while ((eofp = fgets(text, 100, fp)) != NULL 
         && strncmp(text, "<<<<>>>>", 8) != 0)
    {
     if ((n = trim(text)) <= 0)  skip++;
     else
       {
        if ((colon = strchr(text, ':')) != NULL)
	  {
           if (count > 0) put_elpar(key, glist, gcode, length, c);
           *colon = '\0'; strcpy(key, text);
           length = 0; c = 0; count++;
	  }
        else
	  {
           cp1 = strtok(text," \n");
           if (cp1 != NULL)
	     {
              strcpy(&glist[c], cp1);
              cp2 = strtok(NULL," \n");
              if (cp2 != NULL)
		{
                 sscanf(cp2, "%d", &gcode[length]);
                 length++; c += strlen(cp1)+1;
		}
	     }
	  }
       }
    }
  printf("%d element parameter lists stored\n", count);
}

void store_tables()
{
  char *eofp;
  int type_count = 0;

  while ((eofp = fgets(text, 100, fp)) != NULL 
         && strncmp(text, ">>>> The End <<<", 17) != 0)
    {
     if (strncmp(text, "TYPE", 4) == 0)
	 {
          type_table();  type_count++;
	 }
    }
  printf("Total number of type tables stored: %d\n", type_count);
}

int trim(char* s)
{
  int i;
  for (i = strlen(s)-1; i>=0; i--)
    {
     if (s[i] != ' ' && s[i] != '\x9' && s[i] != '\n') break;
    }
  s[++i] = '\0';
  return i;
}
