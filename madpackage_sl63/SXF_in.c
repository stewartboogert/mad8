/* read SXF file into DOOM */
/* #define _call_tree_ */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#ifdef __LINUX__
#include "doomex.h"
#include "doom.h"
#endif
#ifndef __LINUX__
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doomex.h"
#include "/afs/cern.ch/user/h/hansg/public/doom/dev/doom.h"
#endif

/*---------------------------------------------------------------------*
*                                                                      *
*                           CERN                                       *
*                                                                      *
*     European Organization for Nuclear Research                       *
*                                                                      *
*     Program name: DOOM: Mad Object Oriented Database                 *
*                                                                      *
*     Author and contact:   Hans GROTE                                 *
*                           SL Division                                *
*                           CERN                                       *
*                           CH-1211 GENEVA 23                          *
*                           SWITZERLAND                                *
*                      Tel. [041] (022) 767 49 61                      *
*                           Hans.Grote@cern.ch                         *
*                                                                      *
*     Copyright  CERN,  Geneva  1990  -  Copyright  and  any   other   *
*     appropriate  legal  protection  of  this  computer program and   *
*     associated documentation reserved  in  all  countries  of  the   *
*     world.                                                           *
*                                                                      *
*     Organizations collaborating with CERN may receive this program   *
*     and documentation freely and without charge.                     *
*                                                                      *
*     CERN undertakes no obligation  for  the  maintenance  of  this   *
*     program,  nor responsibility for its correctness,  and accepts   *
*     no liability whatsoever resulting from its use.                  *
*                                                                      *
*     Program  and documentation are provided solely for the use  of   *
*     the organization to which they are distributed.                  *
*                                                                      *
*     This program  may  not  be  copied  or  otherwise  distributed   *
*     without  permission. This message must be retained on this and   *
*     any other authorized copies.                                     *
*                                                                      *
*     The material cannot be sold. CERN should be  given  credit  in   *
*     all references.                                                  *
*                                                                      *
*---------------------------------------------------------------------*/

#define ANG_POS 10
#define K0_POS 12
#define LINE_MAX 512
#define MAX_TYPE 11
#define ALIGN_MAX 6
#define FIELD_MAX 100
#define ELEM_SIZE 50
#define ARG_MAX 100
#define TOKEN_MAX 1000
#define SEQU_STEP 10000
#define BUFFER_STEP 10000

int add_name(char*, char*);
void add_to_buffer(char*, char*);
int b_match(int);
void c_replace(char*, char, char);
void finish_elem();
int get_line();
int get_item();
double get_tilt();
int get_token_list(char*, char**);
int get_values(int, int, int, double*);
int in_list(char*, int, char**);
int key_search(char*, char**, int);
char* lower(char*);
void pro_element(int, int);
void pro_part(int, int); /* recursive */
void pro_term();
void set_sxf_env(int);
void store_values(int, int, double*);
void sxf_get_directory();
void sxf_init();
void sxf_inf();
void trim(struct object*);
char* upper(char*);
int version_header();

char           sxf_keyw[] =
    "align|align.dev|aperture|body|body.dev|entry|entry.dev|exit|exit.dev";
int            c_sxf_keyw = 9;

char           sxf_align[] = "al";
int            c_sxf_align = 1;

char           sxf_bdev[] = "kl|kls";
int            c_sxf_bdev = 2;

char           sxf_apert[] = "al|shape|size";
int            c_sxf_apert = 3;

char           begin_name[24] = "BEGIN$                  ";
char           end_name[24]   = "END$                    ";

struct object *psequ,             /* pointer to SEQUENCE object */
              *puse,              /* pointer to USE object */ 
              *pelem,             /* pointer to current ELEMENT object */
              *palign,            /* pointer to current alignment object */
              *pfield,            /* pointer to current field error object */
              *pbase,             /* pointer to current BASE object */
              *align_def,         /* alignment keywords */
              *keyw_def,          /* SXF keyword list */
              *apert_def,         /* aperture keywords */
              *bdev_def,          /* body.dev etc. keywords */
              *pfill,             /* current object being filled */
              *dir_list,          /* MAD directory list */
              *directory,         /* MAD directory */
              *env;               /* environment object with keyword list */


char          sequ_name[KEY_LENGTH],
              key[KEY_LENGTH],
              elem_name[KEY_LENGTH],
              type_name[KEY_LENGTH],
              tag_name[KEY_LENGTH],
              line[LINE_MAX],
              kline[LINE_MAX],
              aux[LINE_MAX];

char*         eofp;
char*         buffer;
char*         kbuffer; /* copy of input buffer for error messages */
char*         current_line_pos = NULL;
char*         last_line_pos = NULL;
char*         token_list[TOKEN_MAX];  /* refers to buffer */

int           sequ_no_elem,        /* no. of elements in sequence */
              na_err = 6,          /* max. no. of alignment errors */
              nf_err = 20,         /* max. no. of field errors/type */
              item_length,         /* length of one item in buffer */
              fill_max,            /* max. no. of values for current item */
              eof_read = 0,        /* flag for eof */
              sxf_end = 0,         /* flag for "// SXF end" read */
              first_token = 0,     /* first token for element description */
              last_token = 0,      /* last token for element description */
              buffer_size = BUFFER_STEP, /* +1 if file correctly terminated */
              closure_error = 0,   /* counter for "{}" closure errors */
              elem_cnt = 0,        /* element count */
              align_cnt = 0,       /* element with align errors count */
              field_cnt = 0,       /* element with field errors count */
              b_level = 0;         /* current brace level */

double        sequ_length = 0,     /* length of  sequence */
              sequ_start = 0, 
              elem_pos,
              elleng,              /* element length */
              ellarc,              /* element arc length */
              zero = 0,
              pi,
              al_errors[ALIGN_MAX],
              fd_errors[FIELD_MAX];

int* idummy;
double* ddummy;

FILE          *inf;

void main(int argc, char *argv[])  /* reads an SXF machine file 
                                      and stores it in a DOOM database */
{
  int snoup = 1;

  if (argc < 3) 
    {
     puts("usage: SXF_in in_file db_name");
     exit(1);
    }
  inf = fopen(argv[1], "r");
  if (inf == NULL)
    {
     printf("Fatal - cannot open input file: %s\n", argv[1]);
     exit(1);
    }
  doom_open(argv[2]);
  sxf_init();
  sxf_inf();
  printf("SXF_in terminated - total number of elements: %d\n", elem_cnt);
  printf("              elements with alignment errors: %d\n", align_cnt);
  printf("              elements with field     errors: %d\n", field_cnt);
  doom_close();
}

int add_name(char* name, char* list_pos)
{
  char* p = list_pos;

  *p++ = '|'; strcpy(p, name);
  return (strlen(name) + 1);
}

void add_to_buffer(char* start, char* end)
{
  char *p = start, *pk, *aux;

  if (item_length + 100 + end - start >= buffer_size)
    {
     buffer_size += BUFFER_STEP;
     aux = (char*) malloc(buffer_size);
     strcpy(aux, buffer); free(buffer); buffer = aux;
     free(kbuffer); kbuffer = (char*) malloc(buffer_size);
    }
  while (p <= end)
    {
     if (*p == '{')  b_level++;
     else if(*p == '}') b_level--;
     if ((pk = strchr("{}[]", *p)) != NULL) buffer[item_length++] = ' ';
     buffer[item_length++] = *p++;
     if (pk != NULL) buffer[item_length++] = ' ';
    }
  buffer[item_length] = '\0';
}

int b_match(int start)
{
  int j, k = 0;

  for (j = start; j <= last_token; j++)
    {
     if (*token_list[j] == '{') k++;
     else if(*token_list[j] == '}') k--;
     if (k == 0)  return j;
    }
  return 0;
}

void c_replace(char* string, char c1, char c2)
{
  char* p = string;

  while(*p != '\0')
    {
     if (*p == c1)  *p = c2; p++;
    }
}

void finish_elem()  /* fixes arc length etc. */
{
  char* mm_list[] = {"RBEND", "SBEND", "QUADRUPOLE", "SEXTUPOLE",
                     "OCTUPOLE", "ELSEPARATOR"};
  int j, rb_flag = strcmp(pelem->base_name, "RBEND"),
         mm_flag = in_list(pelem->base_name, 6, mm_list);
  double angle, ratio, s;

  if (mm_flag == 0)
    {
     if (pelem->a_dble[0] > 0) /* sbend arc length given explicitly */ 
       {
        elleng = pelem->a_dble[0];
       }
     if (pelem->a_dble[K0_POS] < zero ||
         pelem->a_dble[K0_POS+1] < zero) s = -1;
     else          s = 1;
     angle = sqrt(pelem->a_dble[K0_POS]*pelem->a_dble[K0_POS]
            + pelem->a_dble[K0_POS+1]*pelem->a_dble[K0_POS+1]);
     if (angle > zero)
       {
        pelem->a_dble[ANG_POS] = s * angle;
        if (rb_flag == 0) ratio = 2 * sin(angle / 2) / angle;
        else              ratio = 1;
        if (elleng > ellarc)  ellarc = elleng / ratio;
        else                  elleng = ellarc * ratio;
        if (elleng > zero)
          {
           pelem->a_dble[1] = angle / ellarc; /* 1/R */
           for (j = K0_POS; j < pelem->c_int; j++)
             {
              pelem->a_dble[j] = pelem->a_dble[j] * ratio;
             }
          }
       }
     else if(ellarc > elleng) elleng = ellarc;
     if (elleng > zero)
       {
        for (j = K0_POS; j < pelem->c_int; j+=2)
          {
           if (pelem->a_dble[j] != zero || pelem->a_dble[j+1] != zero)
	     {
              pelem->a_dble[j] = pelem->a_dble[j] / elleng;
              pelem->a_dble[j+1] = pelem->a_dble[j+1] / elleng;
	     }
          }
       }
     if ((pelem->a_dble[6] = get_tilt()) != zero) 
          pelem->c_int = pelem->c_int < 7 ? 7 : pelem->c_int;
    }
  /*  if (*tag_name != '\0')  strcpy(pelem->par_name, upper(tag_name)); */
  pelem->a_dble[0] = elleng; if (pelem->c_int == 0)  pelem->c_int = 1;
  pelem->c_dble = pelem->c_char = pelem->c_int;
  for (j = 0; j < pelem->c_int; j++)   
       if (pelem->a_dble[j] != zero) pelem->a_int[j]  = 1;
  for (j = 0; j < pelem->c_char; j++)  pelem->a_char[j] = '|';
  pelem->a_char[pelem->c_char] = '\0';
  trim(pelem);
}

int get_item ()            /* get one item up to ";" or eof */
                           /* returns +1 if ";" found */
                           /*         -1 if ";" not found */
                           /*          0 if eof */
{
  char* sem;

#ifdef _call_tree_
       puts("+++++++ get_item");
#endif
  item_length = 0;
  if (current_line_pos < last_line_pos)
    {
     sem = strchr(current_line_pos, ';');
     if (sem == NULL) /* ";" not found */
       {
        add_to_buffer(current_line_pos, last_line_pos); 
        current_line_pos = last_line_pos;
       }
     else
       {
        add_to_buffer(current_line_pos, sem); 
        current_line_pos = sem + 1;
       }
    }
  else sem = NULL;
  while (sem == NULL && eof_read == 0)
    {
     if (get_line() == 0)  eof_read = 1;
     else
       {
        current_line_pos = line; last_line_pos = line + strlen(line) - 1;
        sem = strchr(current_line_pos, ';');
        if (sem == NULL) /* ";" not found */
          {
           add_to_buffer(current_line_pos, last_line_pos); 
           current_line_pos = last_line_pos;
          }
        else
          {
           add_to_buffer(current_line_pos, sem); 
           current_line_pos = sem + 1;
          }
       }
    }
  strcpy(kbuffer, buffer);
  if (eof_read == 0)  return 1;
  else
    {
     if (item_length == 0) return 0;
     else                  return -1;
    }
}

int get_line ()
{

#ifdef _call_tree_
       puts("+++++++ get_line");
#endif
  do
    {
     eofp = fgets(line, LINE_MAX, inf);
     if (eofp == NULL) return 0;
    }
  while(*line == '#');
  strcpy(kline, line);
  return 1;
}

double get_tilt()
{
  int j, m_flag = strcmp(pelem->base_name, "MULTIPOLE");
  int e_flag = strcmp(pelem->base_name, "ELSEPARATOR");
  double tilt;

  if (e_flag == 0) return atan2(pelem->a_dble[3], pelem->a_dble[2]);
  else if (m_flag != 0)
    {
     for (j = K0_POS; j < pelem->c_int; j+=2) /* get tilt */
       {
        if (pelem->a_dble[j] != zero || pelem->a_dble[j+1] != zero)
          {
           tilt = atan2(pelem->a_dble[j+1], pelem->a_dble[j]);
           if (abs(abs(tilt) - pi) < (double)1.e-6)  tilt = zero;
           return 2 * tilt / (j + 2 - K0_POS);
          }
       }
    }
  else return zero;
}

int get_token_list                 /* returns no. of tokens */
                   (char* text,    /* input: blank separated tokens */
                    char** tokens) /* output: token pointer list */
{
  char* p;
  int j, count = 0;

#ifdef _call_tree_
       puts("+++++++ get_token_list");
#endif
  p = strtok(text," =;\n");
  while (p != NULL)
    {
     tokens[count++] = p;
     p = strtok(NULL," =;\n");
    }
  return count-1;
}

int get_values   /* stores values in array, returns number of last
                    used token, i.e. of number, or "]" */
              (int first,  /* input: first token */
               int last,   /* input: last token */
               int max,    /* max. number of values to store */
               double* val)/* input/output: array for values */
{
  int j, up = first, ret = first;

#ifdef _call_tree_
       puts("+++++++ get_values");
#endif
  for (j = 0; j < max; j++) val[j] = zero;
  if (*token_list[first] == '[')
    {
     for (ret = ++first; ret <= last; ret++)
       {
        if (*token_list[ret] == ']')  break;
       }
     if (ret > last)
       {
        printf("+=+=+= fatal - missing \"]\"in:\n"); 
        puts(kbuffer);
        exit(1);
       }
     up = ret - first > max ? first + max - 1 : ret-1;
    }
  for (j = first; j <= up; j++) sscanf(token_list[j], "%lf", &val[j-first]);
  return ret;
}

int in_list(char* string, int cnt, char** list)
{
  int j;

  for (j = 0; j < cnt; j++)  if(strcmp(string, list[j]) == 0)  return 0;
  return 1;
}

int key_search(     /* sequential search: returns list position or -1 */
    char* carg,     /* input: string to be looked up */
    char* s_list[], /* input: list of character items */
    int  c_list)    /* input: no. of items in s_list */
{
  int j;

  for (j = 0; j < c_list; j++)
    {
     if(strcmp(carg, s_list[j]) == 0)  return j;
    }
    return -1;
}

char* lower(char* s)
{
  char* cp = s;
  while(*cp != '\0') *cp++ = (char) tolower((int)*cp);
  return s;
}

void pro_element (int first, int last) /* processes one element */
{
  int j, k, key_list[] = {1, ELPAR_CODE};

#ifdef _call_tree_
       puts("+++++++ pro_element");
#endif
  make_key(lower(token_list[first+1]), key_list, key);
  if (db_exists(key) == 0)
    {
     printf("-=-=-= fatal - illegal element type: %s", 
            token_list[first+1]);
     exit(1);
    }
  *tag_name = '\0';
  env = pbase = get_obj(key); 
  strcpy(elem_name, token_list[first]);
  strcpy(type_name, token_list[first+1]);
  make_key(token_list[first], key_list, key);
  if (db_exists(key) != 0)
    {
     printf("-=-=-= warning - replacing existing element: %s", 
            token_list[first]);
    }
  pelem = make_obj(elem_name, ELEM_SIZE,  ELEM_SIZE, ELEM_SIZE, 0);
  clean_obj(pelem);
  strcpy(pelem->par_name, upper(type_name));
  strcpy(pelem->base_name, type_name);
  strcpy(pelem->obj_type, "ELEMENT");
  pfill = pelem; /* default: fill element */
  palign = NULL; pfield = NULL;
  elleng = 0; ellarc = 0;
  for (j = first+2; j <= last; j++)
    {
     if (*token_list[j] == '{')
       {
        k = b_match(j);
        pro_part(j+1, k-1);
        j = k;
       }
     else puts(token_list[j]);
    }
  finish_elem(); doom_save(pelem); elem_cnt++;
  n_list_add(n_list_pos(elem_name, dir_list), elem_name, dir_list); 
  if (palign != NULL) 
    {
     doom_save(palign); align_cnt++;
    }
  if (pfield != NULL) 
    {
     trim(pfield); doom_save(pfield); field_cnt++;
    }
}

void pro_part (int first, int last) /* processes one {}-part of an element */
{
  int j, jj, k, kcnt, lp, code;
  double darr[ARG_MAX];

#ifdef _call_tree_
       puts("+++++++ pro_part");
#endif
  for (j = first; j <= last; j++)
    {
     if (*token_list[j] == '{')
       {
        k = b_match(j);
        pro_part(j+1, k-1);
        j = k;
       }
     else  /* here one finds keyword plus value list */
       {
        if (strcmp("at", lower(token_list[j])) == 0)
	  {
           j = get_values(j+1, last, 1, &elem_pos);
	  }
        else if (strcmp("l", lower(token_list[j])) == 0)
	  {
           j = get_values(j+1, last, 1, &elleng);
	  }
        else if (strcmp("tag", lower(token_list[j])) == 0)
	  {
           strcpy(tag_name, token_list[++j]);
	  }
        else
	  {
           lp = key_search(token_list[j], keyw_def->names, keyw_def->c_obj);
           if (lp >= 0) set_sxf_env(lp);
           else
	     {
              lp = key_search(token_list[j], env->names, env->c_obj);
              if (lp < 0)
                {
                 printf("+=+=+= fatal - undefined keyword: %s for type: %s\n", 
                 token_list[j], type_name);
                 prt_obj_full(env);
                 exit(1);
	        }
              code = env->a_int[lp];
              k = j+1;
              j = get_values(k, last, ARG_MAX, darr);
              if (*token_list[k] == '[')  kcnt = j - k - 1;
              else                        kcnt = j + 1 - k;
              store_values(code, kcnt, darr);
	     }
	  }
       }
    }
}

void pro_term ()            /* processes termination record */
{
  int j;
#ifdef _call_tree_
       puts("+++++++ pro_term");
#endif

  last_token = get_token_list(buffer, token_list);
  if (strcmp(lower(token_list[0]), "endsequence") != 0 ||
      strcmp(lower(token_list[1]), "at") != 0)
    {
     printf("-=-=-= warning - no \"endsequence at = ...\" %s\n", 
            "using last element position");
     sequ_length = elem_pos;
    }
  else j = get_values(2, last_token, 1, &sequ_length);
  j = key_search("//", token_list, last_token+1);
  if (j >= 0 && strcmp(lower(token_list[j+1]), "sxf") == 0
      && strcmp(lower(token_list[j+2]), "end") == 0) sxf_end = 1;
}

void set_sxf_env     /*provides the correct objects for the values */
                (int env_type)
{
  int a_key_list[3] = {2, ALIGN_CODE, 1};
  int f_key_list[3] = {2, FIELD_CODE, 1};
  char rec_key[KEY_LENGTH], t_key[KEY_LENGTH];

#ifdef _call_tree_
       puts("+++++++ set_sxf_env");
#endif
  switch(env_type)
    {
     case 0: case 1:
         env = align_def;
         if (palign == NULL)
	   {
            make_key(elem_name, a_key_list, t_key);
            make_dollar_key("UNNAMED_USE", t_key, rec_key);
            palign = make_obj(rec_key, ALIGN_MAX,  ALIGN_MAX, ALIGN_MAX, 0);
            strcpy(palign->par_name, "UNNAMED_USE");
            strcpy(palign->obj_type, "ALIGN_ERROR");
            clean_obj(palign);
           }
         pfill = palign;
         break;
     case 2:
         env = apert_def;
         break;
     case 4: case 6: case 8:
         env = bdev_def;
         if (pfield == NULL)
	   {
            make_key(elem_name, f_key_list, t_key);
            make_dollar_key("UNNAMED_USE", t_key, rec_key);
            pfield = make_obj(rec_key, FIELD_MAX,  FIELD_MAX, FIELD_MAX, 0);
            strcpy(pfield->par_name, "UNNAMED_USE");
            strcpy(pfield->obj_type, "FIELD_ERROR");
            clean_obj(pfield);
           }
         pfill = pfield;
         break;
     default:
         env = pbase;
         break;
    }
}

void sxf_get_directory()
{
  int i, j, lp, len = 16;
  char *c, *p;

  directory = make_obj("DIRECTORY", 0, 0, 16*dir_list->c_obj, 
                       dir_list->c_obj);
  strcpy(directory->par_name, "DIRECTORY");
  strcpy(directory->obj_type, "DIRECTORY");
  c = directory->a_char;
  for (j = 0; j < dir_list->c_obj; j++)
    {
     p = dir_list->names[dir_list->a_int[j]]; lp = strlen(p) + 1;
     for (i = 0; i < lp; i++) *c++ = *p++;
    }
  directory->c_obj = dir_list->c_obj; 
  directory->c_char = dir_list->c_char;
  make_names(directory); doom_save(directory);
}

void sxf_init()
{
  int cl;
  int align_code[] = {0};
  int field_code[] = {2000, 2001};
  buffer = (char*) malloc(buffer_size);
  kbuffer = (char*) malloc(buffer_size);

  pi = 4 * atan((double)1);
  /* make temporary list objects */

  cl = strlen(sxf_align)+1;
  align_def = make_obj("align_object", 1, 0, cl, c_sxf_align);
  fill_obj(align_def, 1, 0, cl, align_code, ddummy, sxf_align);
  align_def->c_obj = align_def->l_obj;
  c_replace(align_def->a_char, '|', '\0'); make_names(align_def);

  cl = strlen(sxf_keyw)+1;
  keyw_def = make_obj("keyw_object", 0, 0, cl, c_sxf_keyw);
  fill_obj(keyw_def, 0, 0, cl, idummy, ddummy, sxf_keyw);
  keyw_def->c_obj = keyw_def->l_obj;
  c_replace(keyw_def->a_char, '|', '\0'); make_names(keyw_def);

  cl = strlen(sxf_bdev)+1;
  bdev_def = make_obj("bdev_object", 2, 0, cl, c_sxf_bdev);
  fill_obj(bdev_def, 2, 0, cl, field_code, ddummy, sxf_bdev);
  bdev_def->c_obj = bdev_def->l_obj;
  c_replace(bdev_def->a_char, '|', '\0'); make_names(bdev_def);

  cl = strlen(sxf_apert)+1;
  apert_def = make_obj("apert_object", 0, 0, cl, c_sxf_apert);
  fill_obj(apert_def, 0, 0, cl, idummy, ddummy, sxf_apert);
  apert_def->c_obj = apert_def->l_obj;
  c_replace(apert_def->a_char, '|', '\0'); make_names(apert_def);
  dir_list = make_obj("DIR_LIST", SEQU_STEP, 0, 16*SEQU_STEP, SEQU_STEP);
  strcpy(dir_list->par_name, "DIR_LIST");
  strcpy(dir_list->obj_type, "DIR_LIST");
}

void sxf_inf ()  /* steering routine */
{
  int j, new_leng, one = 1, key_list[] = {1, SEQU_CODE};

#ifdef _call_tree_
       puts("+++++++ sxf_inf");
#endif
  if (version_header() == 0)
    {
     printf("+=+=+= fatal - SXF header missing or wrong\n");
     exit(1);
    }
  if (get_item() <= 0)
    {
     printf("+=+=+= fatal - no item found ending with \";\"\n");
     exit(1);
    }
  if((last_token = get_token_list(buffer, token_list)) < 3 ||
     strcmp(lower(token_list[1]), "sequence") !=0 || *token_list[2] != '{')

    {
     printf("+=+=+= fatal - sequence definition missing\n");
     exit(1);
    }
  strcpy(sequ_name, token_list[0]);
  make_key(sequ_name, key_list, key);
  if (db_exists(key) != 0)
    {
     printf("-=-=-= warning - sequence %s replaced in d.b.", sequ_name);
    }
  strcpy(&begin_name[6], sequ_name); strcpy(&end_name[4], sequ_name);
  psequ = make_obj(key, SEQU_STEP, SEQU_STEP, 16*SEQU_STEP, SEQU_STEP);
  strcpy(psequ->par_name, "UNNAMED_USE");
  strcpy(psequ->obj_type, "EX_SEQUENCE");
  fill_obj(psequ, 1, 1, strlen(begin_name), &one, &sequ_start, begin_name); 
  /* add an object UNNAMED_USE with the sequence name */
  puse = make_obj("UNNAMED_USE", 0, 0, 17, 1);
  strcpy(puse->par_name, "USE");
  strcpy(puse->obj_type, "USE");
  fill_obj(puse, 0, 0, strlen(sequ_name)+1, idummy, ddummy, sequ_name);
  puse->c_char = strlen(sequ_name)+1;
  puse->c_obj = 1;
  first_token = 3;
  do
    {
     if (b_level != 1)
       {
        printf("-=-=-= warning - skipping item with unmatched \"{}\":\n");
        puts(kbuffer); b_level = 1;
        if (++closure_error == 10)
          {
           printf("+=+=+= fatal - 10 \"{}\" closure errors\n");
           exit(1);
          }
       }
     else
       {
        pro_element(first_token, last_token);
	psequ->a_int[psequ->c_int++] = 1;      /* occurrence count */
	psequ->a_dble[psequ->c_dble++] = elem_pos;  /* position */
        psequ->c_char += add_name(elem_name, &psequ->a_char[psequ->c_char]);
        if (psequ->c_int == psequ->l_int)
	  {
           new_leng = psequ->l_int + SEQU_STEP;
           grow_obj(psequ, new_leng, new_leng, 0, new_leng);
	  }
        if (psequ->c_char + 16 >= psequ->l_char)
	  {
           new_leng = psequ->l_char + 16*SEQU_STEP;
           grow_obj(psequ, 0, 0, new_leng, 0);
	  }
        first_token = 0;
        if(get_item() < 0) pro_term();
        else last_token = get_token_list(buffer, token_list);
       }
    }
  while (eof_read == 0 && sxf_end == 0);
  psequ->a_int[psequ->c_int++] = 1;      /* occurrence count */
  psequ->a_dble[psequ->c_dble++] = sequ_length;  /* end position */
  psequ->c_char += add_name(end_name, &psequ->a_char[psequ->c_char])+1;
  psequ->c_obj = psequ->c_int;
  trim(psequ);
  c_replace(psequ->a_char, '|', '\0'); make_names(psequ);
  n_list_add(n_list_pos("UNNAMED_USE", dir_list), "UNNAMED_USE", dir_list); 
  doom_save(puse); doom_save(psequ);
  sxf_get_directory();
  if (b_level > 0)
     printf("+=+=+= warning - %d missing '}'\n", b_level);
  if (sxf_end == 0)
     printf("+=+=+= warning - missing \"// SXF end\"\n");
}

void store_values(             /* puts values into object arrays */
                  int code,    /* code (type, position) */
                  int cnt,     /* number of values present */
                  double* val) /* values */
{
  int j, k, mp = 0, type = code / 1000, pos = code % 1000;

  if (type == 0)     
    {
     for (j = 0; j < cnt; j++) 
       {
        mp = pos+j; pfill->a_dble[mp] = val[j];
       }
    }
  else if(type == 3) ellarc = *val;
  else 
    {
     for (j = 0, k = 0; j < cnt; j++, k+=2) 
       { 
        mp = pos+k; pfill->a_dble[mp] = val[j];
       }
    }
  if (pfill->c_int < ++mp)  pfill->c_int = mp;
  pfill->c_dble = pfill->c_int;
}

void trim(struct object* p)
{
  p->l_int = p->c_int; p->l_dble = p->c_dble;
  p->l_char = p->c_char; p->l_obj = p->c_obj;
}

char* upper(char* s)
{
  char* cp = s;
  while(*cp != '\0') *cp++ = (char) toupper((int)*cp);
  return s;
}

int version_header ()            /* processes and checks header */
{
  int j = 0;
  char* header[] = {"//", "sxf", "version", "1.0"};

#ifdef _call_tree_
       puts("+++++++ version_header");
#endif
  if (get_line() == 0)  return 0;
  if(get_token_list(line, token_list) != 3) return 0;
  for (j = 0; j < 3; j++)
    {
     if (strcmp(header[j], lower(token_list[j])) != 0) return 0;
    }
  if (strcmp(header[3], lower(token_list[3])) != 0)
      {
       printf("-=-=-= warning - header version number = %s\n", 
            token_list[3]);
       printf("                 different from program version number = %s\n", 
            header[3]);
      }
  return 1;
}
