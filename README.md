#pyMad8#

A python package containing both utilities for processing, converting and analysing Mad8 output 

## Authors ##
* S. Boogert
* W. Parker

## Setup ##
The module currently requires no setup and can be used by adding the pybdsim directory to your python path.

git clone http://bitbucket.org/stewartboogert/mad8 .

-> edit your terminal (perhaps bash) profile


```
#!csh

$PYTHONPATH=$PYTHONPATH:/path/to/where/you/put/pymad8

```


```
#!python
python
python> import pymad8
python> o = pymad.Mad8.OutputReader()
python> [c,t] = o.LoadOutput("./acc_twiss.tape","twiss") 
```

# Dependencies #
* python2.7
* ipython
* numpy
* scipy
* matplotlib 
* [fortranformat](https://pypi.python.org/pypi/fortranformat)